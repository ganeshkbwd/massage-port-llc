package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.MyAppointmentAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.MyScheduleAppointments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class MyAppointmentActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    ArrayList<MyScheduleAppointments> msaList = new ArrayList<MyScheduleAppointments>();
    ListView appointmentLV;
    Button myProfile, myAppointment, mapView;
    Toolbar toolbar;
    private static boolean doubleBackToExitPressedOnce = false;
    TextView noAppScheduled;
    int login, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_appointment);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();

        appointmentLV = (ListView) findViewById(R.id.scheduleAppLV);
        noAppScheduled = (TextView) findViewById(R.id.noReviewTV);

        if (CheckConnectivity.checkInternetConnection(MyAppointmentActivity.this)) {
            scheduledAppointmentWebService(user_id);
        } else {
            CheckConnectivity.noNetMeg(MyAppointmentActivity.this);
        }


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void scheduledAppointmentWebService(final int user_id) {


        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.myAppointmentsCustomer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                noAppScheduled.setVisibility(View.GONE);
                                appointmentLV.setVisibility(View.VISIBLE);
                                msaList.clear();
                                JSONArray jarr = jobj.getJSONArray("response");
                                for (int i = 0; i < jarr.length(); i++) {
                                    MyScheduleAppointments msa = new MyScheduleAppointments();
                                    msa.setBooking_id(jarr.getJSONObject(i).getInt("booking_id"));
                                    msa.setBooking_type(jarr.getJSONObject(i).getString("booking_type"));
                                    msa.setDate(jarr.getJSONObject(i).getString("date"));
                                    msa.setHour(jarr.getJSONObject(i).getString("hour"));
                                    msa.setMinute(jarr.getJSONObject(i).getString("minute"));
                                    msa.setPeriod(jarr.getJSONObject(i).getString("period"));
                                    msa.setCustomer_id(jarr.getJSONObject(i).getInt("customer_id"));
                                    msa.setTherapist_id(jarr.getJSONObject(i).getInt("therapist_id"));
                                    msa.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    msa.setCustomer_address(jarr.getJSONObject(i).getString("customer_address"));
                                    msa.setTherapist_name(jarr.getJSONObject(i).getString("therapist_name"));
                                    msa.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));
                                    msa.setFull_address(jarr.getJSONObject(i).getString("full_address"));
                                    msa.setAvg_rating(jarr.getJSONObject(i).getString("avg_rating"));
                                    msa.setTotal_reviews(jarr.getJSONObject(i).getString("total_reviews"));

                                    msaList.add(msa);


                                }
                                appointmentLV.setAdapter(new MyAppointmentAdapter(MyAppointmentActivity.this, msaList));

//                                Toast.makeText(MyAppointmentActivity.this, "Your appointment booked successfully.", Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();
//                                Intent intent = new Intent(MyAppointmentActivity.this, MapsActivity.class);
//                                startActivity(intent);
//                                finish();

                            } else {

                                noAppScheduled.setVisibility(View.VISIBLE);
                                appointmentLV.setVisibility(View.INVISIBLE);
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message");
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MyAppointmentActivity.this,ConstantVal.MESSAGE_CODE,Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(MyAppointmentActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("customer_id", String.valueOf(user_id));

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.viewMapBTN:
                ConstantVal.BACK = 1;
                intent = new Intent(MyAppointmentActivity.this, MapsActivity.class);

                startActivity(intent);
                finish();
//                Toast.makeText(this, "Under Development....", Toast.LENGTH_SHORT).show();
                break;
            case R.id.myAppBTN:
                Toast.makeText(this, "You are currently on My Appointments", Toast.LENGTH_SHORT).show();
                break;
            case R.id.myProfileBTN:
                intent = new Intent(MyAppointmentActivity.this, CustomerProfileActivity.class);
                startActivity(intent);
                finish();
//                Toast.makeText(this, "Under Development....", Toast.LENGTH_SHORT).show();
                break;
            case R.id.msgBTN:
                intent = new Intent(MyAppointmentActivity.this,TherapistsActivity.class);
                startActivity(intent);
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit from Massage Port,LLC App", Toast.LENGTH_SHORT)
                .show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 1)
            getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
