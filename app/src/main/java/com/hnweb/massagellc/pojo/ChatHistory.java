package com.hnweb.massagellc.pojo;

/**
 * Created by neha on 11/11/2016.
 */
public class ChatHistory {
    String dmsg_id, from_userid, to_user_id, message, created_datetime;

    public String getDmsg_id() {
        return dmsg_id;
    }

    public void setDmsg_id(String dmsg_id) {
        this.dmsg_id = dmsg_id;
    }

    public String getFrom_userid() {
        return from_userid;
    }

    public void setFrom_userid(String from_userid) {
        this.from_userid = from_userid;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }
}
