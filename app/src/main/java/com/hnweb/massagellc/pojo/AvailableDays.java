package com.hnweb.massagellc.pojo;

import java.io.Serializable;

/**
 * Created by neha on 9/24/2016.
 */
public class AvailableDays implements Serializable {

    int time_id;
    int therapist_id;
    int hour;
    int minute;
    String period;
    String date;
    String crated_date;
    String last_updated_date;

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getTime_id() {
        return time_id;
    }

    public void setTime_id(int time_id) {
        this.time_id = time_id;
    }

    public int getTherapist_id() {
        return therapist_id;
    }

    public void setTherapist_id(int therapist_id) {
        this.therapist_id = therapist_id;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCrated_date() {
        return crated_date;
    }

    public void setCrated_date(String crated_date) {
        this.crated_date = crated_date;
    }

    public String getLast_updated_date() {
        return last_updated_date;
    }

    public void setLast_updated_date(String last_updated_date) {
        this.last_updated_date = last_updated_date;
    }




}
