package com.hnweb.massagellc.pojo;

/**
 * Created by neha on 9/26/2016.
 */
public class CustomerReview {
    String therapist_name;
    String therapist_photo;
    String avg_rating;
    int review_id;
    int customer_id;
    int therapist_id;
    String comment;
    String stars;
    String created_datetime;
    String reply_text;
    String customer_name;

    public String getTherapist_name() {
        return therapist_name;
    }

    public void setTherapist_name(String therapist_name) {
        this.therapist_name = therapist_name;
    }

    public String getTherapist_photo() {
        return therapist_photo;
    }

    public void setTherapist_photo(String therapist_photo) {
        this.therapist_photo = therapist_photo;
    }

    public String getAvg_rating() {
        return avg_rating;
    }

    public void setAvg_rating(String avg_rating) {
        this.avg_rating = avg_rating;
    }

    public int getReview_id() {
        return review_id;
    }

    public void setReview_id(int review_id) {
        this.review_id = review_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public int getTherapist_id() {
        return therapist_id;
    }

    public void setTherapist_id(int therapist_id) {
        this.therapist_id = therapist_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getCreated_datetime() {
        return created_datetime;
    }

    public void setCreated_datetime(String created_datetime) {
        this.created_datetime = created_datetime;
    }

    public String getReply_text() {
        return reply_text;
    }

    public void setReply_text(String reply_text) {
        this.reply_text = reply_text;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }
}
