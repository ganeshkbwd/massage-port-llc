package com.hnweb.massagellc.pojo;

import java.io.Serializable;

/**
 * Created by neha on 9/28/2016.
 */
public class CustomerProfile implements Serializable {

    int user_id;
    String full_name;
    String full_address;
    String email_address;

    public String getFull_address() {
        return full_address;
    }

    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }


}
