package com.hnweb.massagellc.pojo;

/**
 * Created by neha on 12/26/2016.
 */

public class TherapistToTherapist {

    String to_user_id,therapist_name,profile_photo;

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getTherapist_name() {
        return therapist_name;
    }

    public void setTherapist_name(String therapist_name) {
        this.therapist_name = therapist_name;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(String profile_photo) {
        this.profile_photo = profile_photo;
    }
}
