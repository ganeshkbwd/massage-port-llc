package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.ChatHistoryAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.ChatHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class MessageToCustomerActivity extends AppCompatActivity implements View.OnClickListener {

    String id, name, photo;
    Toolbar toolbar;
    ImageView therapist_profileIV;
    TextView customerNameTV;
    RecyclerView recyclerView;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id;
    ArrayList<ChatHistory> chatHistoryArrayList = new ArrayList<ChatHistory>();
    EditText msgET;
    Button replyBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_to_customer);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        id = getIntent().getExtras().getString("ID");
//        Log.e("IDD :- ", id);
        name = getIntent().getExtras().getString("NAME");

        init();

        chatWebservice();


    }

    public void init() {


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        therapist_profileIV = (ImageView) findViewById(R.id.therapist_profileIV);
        customerNameTV = (TextView) findViewById(R.id.customerNameTV);
        msgET = (EditText) findViewById(R.id.msgET);
        replyBTN = (Button) findViewById(R.id.replyBTN);

        therapist_profileIV.setVisibility(View.GONE);
        customerNameTV.setText(name);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void chatWebservice() {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.chatHistoryUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");


                            if (ConstantVal.MESSAGE_CODE == 1) {
//
                                chatHistoryArrayList.clear();
                                JSONArray jarr = jobj.getJSONArray("response");
                                for (int i = 0; i < jarr.length(); i++) {

                                    ChatHistory c = new ChatHistory();
                                    c.setDmsg_id(jarr.getJSONObject(i).getString("dmsg_id"));
                                    c.setFrom_userid(jarr.getJSONObject(i).getString("from_userid"));
                                    c.setTo_user_id(jarr.getJSONObject(i).getString("to_user_id"));
                                    c.setMessage(jarr.getJSONObject(i).getString("message"));
                                    c.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));

                                    chatHistoryArrayList.add(c);


                                }
                                recyclerView.setAdapter(new ChatHistoryAdapter(MessageToCustomerActivity.this, chatHistoryArrayList, user_id, name));

                                ConstantVal.progressDialog.dismiss();


                            } else {
                                ConstantVal.MESSAGE = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(MessageToCustomerActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(MessageToCustomerActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("my_user_id", String.valueOf(user_id));
                params.put("other_user_id", id);


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    public void sendMsgWebservice(final String msg) {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.sendMsg,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");


                            if (ConstantVal.MESSAGE_CODE == 1) {
                                msgET.setText("");
                                ConstantVal.progressDialog.dismiss();
                                chatWebservice();

                            } else {
                                ConstantVal.MESSAGE = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(MessageToCustomerActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(MessageToCustomerActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("from_userid", String.valueOf(user_id));
                params.put("to_user_id", id);
                params.put("message", msg);


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.replyBTN:

                if (msgET.getText().toString().trim().length() > 0) {
                    if (CheckConnectivity.checkInternetConnection(this)) {
                        sendMsgWebservice(msgET.getText().toString().trim());
                    } else {
                        CheckConnectivity.noNetMeg(this);
                    }

                }


                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 2)
            getMenuInflater().inflate(R.menu.therapist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }

}
