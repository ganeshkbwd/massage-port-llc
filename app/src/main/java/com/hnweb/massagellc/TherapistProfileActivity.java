package com.hnweb.massagellc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.TimingsAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.AvailableDays;
import com.hnweb.massagellc.pojo.AvailableTimings;
import com.hnweb.massagellc.pojo.CustomerReview;
import com.hnweb.massagellc.pojo.ShowTherapistData;
import com.squareup.picasso.Picasso;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TherapistProfileActivity extends AppCompatActivity implements View.OnClickListener {
    TextView fullname, speciality, rates, status;
    ImageView profile;
    Button viewReview;
    RatingBar rating;
    CustomCalendarView calendarView;
    ArrayList<ShowTherapistData> filelist;
    int position;
    Toolbar toolbar;
    ArrayList<AvailableDays> availableDays = new ArrayList<AvailableDays>();
    ArrayList<AvailableTimings> availableTimings = new ArrayList<AvailableTimings>();
    ArrayList<CustomerReview> customerReview = new ArrayList<CustomerReview>();
    Geocoder geocoder;
    List<Address> addresses;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    String address = "", city = "", state, country = "", postalCode, subLocality;
    String bookType;
    ArrayList<String> distinctAvailableDatesList = new ArrayList<String>();
    int login, user_id;
    Methodes methodes = new Methodes();
    Button loginHere, msgBTN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapist_profile);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        loginHere = (Button) findViewById(R.id.loginBTN);
        msgBTN = (Button) findViewById(R.id.msgBTN);

        getFromBundle();
        setCalenderView();

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    private void getDistinctAvailableDates() {
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getDistinctAvailableDates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                distinctAvailableDatesList.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String distinct_date = jsonArray.getJSONObject(i).getString("date");
//                                    String[] month = distinct_date.split("-");
//
//                                    Calendar c = Calendar.getInstance();
//                                    int year = c.get(Calendar.YEAR);
//                                    int cur_month = c.get(Calendar.MONTH);
//                                    if (cur_month == Integer.parseInt(month[1])) {
                                    distinctAvailableDatesList.add(distinct_date);
//                                    }


                                }
                                String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                                Log.i("Current Date is", date);
                                calendarView.setNewDateColor(distinctAvailableDatesList, date);
//                                calendarView.setSelectedDayGreen(distinctAvailableDatesList);

                                ConstantVal.progressDialog.dismiss();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(filelist.get(position).getUser_id()));


                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);
    }

//    private void webserviceAvialSchedule() {
//        ConstantVal.progressDialog = new ProgressDialog(this);
//        ConstantVal.progressDialog.setMessage("Please wait ....");
//        ConstantVal.progressDialog.show();
//
//        RequestQueue queue = Volley.newRequestQueue(this);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getAvailableDates,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            Log.e("RESPONSE", response);
//                            JSONObject jobj = new JSONObject(response);
//                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
//                            if (ConstantVal.MESSAGE_CODE == 1) {
//                                JSONArray jsonArray = jobj.getJSONArray("response");
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    AvailableDays ad = new AvailableDays();
//                                    ad.setTime_id(jsonArray.getJSONObject(i).getInt("time_id"));
//                                    ad.setTherapist_id(jsonArray.getJSONObject(i).getInt("therapist_id"));
//                                    ad.setHour(jsonArray.getJSONObject(i).getInt("hour"));
//                                    ad.setMinute(jsonArray.getJSONObject(i).getInt("minute"));
//                                    ad.setPeriod(jsonArray.getJSONObject(i).getString("period"));
//                                    ad.setDate(jsonArray.getJSONObject(i).getString("date"));
//                                    ad.setCrated_date(jsonArray.getJSONObject(i).getString("created_date"));
//                                    ad.setLast_updated_date(jsonArray.getJSONObject(i).getString("last_updated_date"));
//
//                                    availableDays.add(ad);
//
//                                }
//
//
////                                Log.e("LIST", therapistDataList.toString());
//
//                                ConstantVal.progressDialog.dismiss();
////                                settingsDialog.dismiss();
////                                Toast.makeText(context, ConstantVal.RESPONSE, Toast.LENGTH_SHORT).show();
//
//                            } else {
//                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
//                                ConstantVal.progressDialog.dismiss();
//
////                                Toast.makeText(SelectionActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                ConstantVal.progressDialog.dismiss();
//                Toast.makeText(TherapistProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                //Converting Bitmap to String
//                Map<String, String> params = new Hashtable<String, String>();
//                params.put("user_id", String.valueOf(filelist.get(position).getUser_id()));
//
//
//                //returning parameters
//                return params;
//            }
//        };
//
//        queue.add(stringRequest);
//    }

    public void setAvailTimeDialog(final ArrayList<AvailableTimings> availableTimings, String date) {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.appoitment_schedule_layout
                , null));
        settingsDialog.setCancelable(false);
        ListView timeLV = (ListView) settingsDialog.findViewById(R.id.appointmentScheduleLV);
        Button bookBTN = (Button) settingsDialog.findViewById(R.id.bookBTN);
        Button cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);
        ConstantVal.selectedPosition = -1;

        timeLV.setAdapter(new TimingsAdapter(availableTimings, TherapistProfileActivity.this));
        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });

        bookBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConstantVal.selectedPosition != -1) {
                    if (login == 1) {
                        settingsDialog.dismiss();

                        bookingConfirmDialog(filelist, position + "", availableTimings, ConstantVal.selectedPosition + "");

                    } else {

                        ConstantVal.myFileList = filelist;
                        ConstantVal.therapistPosition = String.valueOf(position);
                        ConstantVal.myAvialtime = availableTimings;

                        Intent intent = new Intent(TherapistProfileActivity.this, SignInActivity.class);
                        intent.putExtra("ComeFrom", "Book");
                        startActivity(intent);
                        finish();
                        settingsDialog.dismiss();
                    }

                } else {

                    Toast.makeText(TherapistProfileActivity.this, "Please select appointment time.", Toast.LENGTH_SHORT).show();
                }

            }
        });


        settingsDialog.show();


    }

    public void callDistinctAvailDatesService() {
        if (CheckConnectivity.checkInternetConnection(TherapistProfileActivity.this)) {
            getDistinctAvailableDates();
        } else {
            CheckConnectivity.noNetMeg(TherapistProfileActivity.this);
        }
    }

    public void getFromBundle() {
        Bundle b = getIntent().getExtras();

        if (b != null) {
            if (b.getString("ComeFrom").equalsIgnoreCase("Maps")) {
//                b.getString()
                filelist = (ArrayList<ShowTherapistData>) b.getSerializable("List");
                String position1 = b.getString("Position");
                position = Integer.parseInt(position1);

                callDistinctAvailDatesService();
                init();
//                webserviceAvialSchedule();

            } else if (b.getString("ComeFrom").equalsIgnoreCase("SignIn")) {
                filelist = ConstantVal.myFileList;
                String therapistPosition = ConstantVal.therapistPosition;
//                position = Integer.parseInt(therapistPosition);
                availableTimings = ConstantVal.myAvialtime;
                String position = String.valueOf(ConstantVal.selectedPosition);

                callDistinctAvailDatesService();
                init();
//                setCalenderView();
                bookingConfirmDialog(filelist, therapistPosition, availableTimings, position);

            } else if (b.getString("ComeFrom").equalsIgnoreCase("Review")) {
                filelist = ConstantVal.myFileList;
//                String therapistPosition = ConstantVal.therapistPosition;
//                position = Integer.parseInt(therapistPosition);
//                availableTimings = ConstantVal.myAvialtime;
                position = Integer.parseInt(ConstantVal.therapistPosition);

                callDistinctAvailDatesService();
                init();
            } else if (b.getString("ComeFrom").equalsIgnoreCase("TP")) {
                filelist = ConstantVal.myFileList;
//                String therapistPosition = ConstantVal.therapistPosition;
//                position = Integer.parseInt(therapistPosition);
//                availableTimings = ConstantVal.myAvialtime;
                position = Integer.parseInt(ConstantVal.therapistPosition);

                callDistinctAvailDatesService();
                init();
            }
        }


    }

    public String getAddressFromLatLong(double lattitude, double longitude) {
        String fulladdress = "";
        geocoder = new Geocoder(TherapistProfileActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lattitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            subLocality = addresses.get(0).getSubLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        fulladdress = address + "," + subLocality + "," + city + "," + state + "," + country;
        return fulladdress;
    }

    public void bookingConfirmDialog(final ArrayList<ShowTherapistData> filelist, final String therapistPosition, ArrayList<AvailableTimings> availableTimings, String position) {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.confirm_booking_dialog
                , null));
        double lattitude = Double.parseDouble(filelist.get(Integer.parseInt(therapistPosition)).getLatitude());
        double longitude = Double.parseDouble(filelist.get(Integer.parseInt(therapistPosition)).getLongitude());
        Log.e("latLong", lattitude + "  " + longitude);


        final RadioGroup radioGroup = (RadioGroup) settingsDialog.findViewById(R.id.radioGroup);
        RadioButton ywcRBT = (RadioButton) settingsDialog.findViewById(R.id.ywcRBTN);
        RadioButton iwcRBT = (RadioButton) settingsDialog.findViewById(R.id.iwcRBTN);
        TextView appDateTimeTV = (TextView) settingsDialog.findViewById(R.id.appDateTimeTV);
        final String date = availableTimings.get(Integer.parseInt(position)).getDate();
        String hour = String.valueOf(availableTimings.get(Integer.parseInt(position)).getHour());
        String minute = String.valueOf(availableTimings.get(Integer.parseInt(position)).getMinute());
        final String period = availableTimings.get(Integer.parseInt(position)).getPeriod();
        final EditText ywcET = (EditText) settingsDialog.findViewById(R.id.ywcET);
//        ywcET.setEnabled(false);
        EditText iwcET = (EditText) settingsDialog.findViewById(R.id.iwcET);
//        ywcET.setClickable(true);
        iwcET.getBackground().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);

        if (Integer.parseInt(hour) < 10) {
            hour = "0" + hour;
        }
        if (Integer.parseInt(minute) < 10) {
            minute = "0" + minute;
        }
        String finalApp = date + " at " + hour + " : " + minute + " " + period;
        appDateTimeTV.setText(finalApp);

        double myCurrLat = methodes.myLocation(TherapistProfileActivity.this).getLatitude();

        double myCurrLongi = methodes.myLocation(TherapistProfileActivity.this).getLongitude();
        Log.e("MYCURLATLONG", myCurrLat + "," + myCurrLongi);
//        ywcRBT.setText("You will come to me at" + "\n" + "(" + getAddressFromLatLong(lattitude, longitude) + ")");
        ywcET.setText(getAddressFromLatLong(ConstantVal.lastKnownLat, ConstantVal.lastKnownLongi));
        ywcET.setSelection(ywcET.getText().length());
        Button okBTN = (Button) settingsDialog.findViewById(R.id.okBTN);
        final Button cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);
        final int finalHour = Integer.parseInt(hour);
        final int finalMinute = Integer.parseInt(minute);
        okBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Methodes methodes = new Methodes();
                String customerAddress = ywcET.getText().toString().trim();
                if (customerAddress.contains("null")) {
                    Toast.makeText(TherapistProfileActivity.this, "Please Provide Valid address", Toast.LENGTH_SHORT).show();
                } else {
                    int checkedId = radioGroup.getCheckedRadioButtonId();
                    RadioButton radioBTN = (RadioButton) settingsDialog.findViewById(checkedId);
                    if (checkedId == -1) {

                        Toast.makeText(TherapistProfileActivity.this, "Please select one option", Toast.LENGTH_SHORT).show();
                    } else {
                        if (radioBTN.getId() == R.id.ywcRBTN) {
                            bookType = "uwctma";
//
                        } else if (radioBTN.getId() == R.id.iwcRBTN) {
                            bookType = "iwctu";
                        }

                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

                        int user_id = sharedPreferences.getInt("user_id", 0);

                        if (customerAddress.length() > 0) {
                            customerAddress = ywcET.getText().toString().trim();
                        }

//                    customerAddress = getAddressFromLatLong(methodes.myLocation(TherapistProfileActivity.this).getLatitude(),
//                            methodes.myLocation(TherapistProfileActivity.this).getLongitude());
                        int therapist_id = filelist.get(Integer.parseInt(therapistPosition)).getUser_id();
                        if (CheckConnectivity.checkInternetConnection(TherapistProfileActivity.this)) {
                            confirmedBookingWebService(user_id, therapist_id, date, finalHour, finalMinute, period, customerAddress, bookType);
                        } else {
                            CheckConnectivity.noNetMeg(TherapistProfileActivity.this);
                        }


                        settingsDialog.dismiss();
                    }


                }


            }
        });

        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();

            }
        });


        settingsDialog.show();

    }

    public void confirmedBookingWebService(final int user_id, final int therapist_id, final String date, final int hour, final int minute, final String period, final String customerAddress, final String bookType) {


        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.bookingRequestByCutomer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {

                                Toast.makeText(TherapistProfileActivity.this, "Your appointment booked successfully.", Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();
                                Intent intent = new Intent(TherapistProfileActivity.this, MyAppointmentActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("customer_id", String.valueOf(user_id));
                params.put("therapist_id", String.valueOf(therapist_id));
                params.put("date", date);
                params.put("hour", String.valueOf(hour));
                params.put("minute", String.valueOf(minute));
                params.put("period", period);
                params.put("customer_address", customerAddress);
                params.put("booking_type", bookType);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void init() {
        fullname = (TextView) findViewById(R.id.fullNameTV);
        rating = (RatingBar) findViewById(R.id.ratingTV);
        speciality = (TextView) findViewById(R.id.specialityTV);
        rates = (TextView) findViewById(R.id.ratesTV);
        status = (TextView) findViewById(R.id.statusTV);
        profile = (ImageView) findViewById(R.id.profileIV);
        viewReview = (Button) findViewById(R.id.viewReviewBTN);
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);

        speciality.setMovementMethod(new ScrollingMovementMethod());
        rates.setMovementMethod(new ScrollingMovementMethod());

        LayerDrawable stars = (LayerDrawable) rating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        viewReview.setOnClickListener(this);

//        Drawable progress = rating.getProgressDrawable();
//        DrawableCompat.setTint(progress, Color.WHITE);

        setData();
    }

    public void setData() {
        try {
            fullname.setText(filelist.get(position).getFull_name());
            speciality.setText(filelist.get(position).getSpeciality());
            rates.setText(filelist.get(position).getRates());
            rating.setRating(Float.parseFloat(filelist.get(position).getAvg_rating()));
            if (filelist.get(position).getAvailable_status().equalsIgnoreCase("Yes")) {
                status.setText("This Massage Therapist is Avialable Now!!!");
            } else {
                status.setText("");
            }

            if (!filelist.get(position).getProfile_photo().equals("")) {
                Picasso.with(this).load(filelist.get(position).getProfile_photo()).into(profile);
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewReviewBTN:
                ConstantVal.myFileList = filelist;
                ConstantVal.therapistPosition = String.valueOf(position);
                Intent intent = new Intent(this, ReviewCustomerSideActivity.class);
                intent.putExtra("ComeFrom", "Book");
                intent.putExtra("TherapistID", String.valueOf(filelist.get(position).getUser_id()));

                startActivity(intent);
//                finish();

                break;
            case R.id.loginBTN:


                break;
            case R.id.msgBTN:
                if (login == 1) {
                    loginHere.setVisibility(View.GONE);
                    msgBTN.setVisibility(View.VISIBLE);

                    Intent intent1 = new Intent(TherapistProfileActivity.this, TherapistsActivity.class);
                    startActivity(intent1);
                    finish();

                } else {
                    loginHere.setVisibility(View.GONE);
                    msgBTN.setVisibility(View.GONE);

                    ConstantVal.myFileList = filelist;
                    ConstantVal.therapistPosition = String.valueOf(position);
                    Intent i = new Intent(this, SignInActivity.class);
                    i.putExtra("ComeFrom", "TP");
                    startActivity(i);
                }

                break;
        }

    }


    public void setCalenderView() {
        //Initialize calendar with date
        final Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.SUNDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);
        calendarView.markDayAsCurrentDay(currentCalendar);
        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);

        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();

                if (!Methodes.isPastDay(date)) {
//                Toast.makeText(TherapistProfileActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
                    ConstantVal.date_selected = df.format(date);
                    if (distinctAvailableDatesList.contains(ConstantVal.date_selected)) {
                        if (CheckConnectivity.checkInternetConnection(TherapistProfileActivity.this)) {
                            callweservicesToBook(df.format(date));
                        } else {
                            CheckConnectivity.noNetMeg(TherapistProfileActivity.this);
                        }

                    } else {
                        Toast.makeText(TherapistProfileActivity.this, "Not Available for Booking.", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(TherapistProfileActivity.this, "Not Available for Booking because you selected past date.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onMonthChanged(Date date) {
//
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                Log.e("Date", df.format(date));
                calendarView.setNewDateColor(distinctAvailableDatesList, df.format(date));
            }
        });
    }


    public void callweservicesToBook(final String date) {
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getAvailableTimings,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                availableTimings.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    AvailableTimings at = new AvailableTimings();
                                    at.setTime_id(jsonArray.getJSONObject(i).getInt("time_id"));
                                    at.setTherapist_id(jsonArray.getJSONObject(i).getInt("therapist_id"));
                                    at.setHour(jsonArray.getJSONObject(i).getInt("hour"));
                                    at.setMinute(jsonArray.getJSONObject(i).getInt("minute"));
                                    at.setPeriod(jsonArray.getJSONObject(i).getString("period"));
                                    at.setDate(jsonArray.getJSONObject(i).getString("date"));
                                    at.setCrated_date(jsonArray.getJSONObject(i).getString("created_date"));
                                    at.setLast_updated_date(jsonArray.getJSONObject(i).getString("last_updated_date"));
                                    at.setStatus(jsonArray.getJSONObject(i).getString("status"));

                                    availableTimings.add(at);

                                }
                                setAvailTimeDialog(availableTimings, date);
//                                Log.e("LIST", therapistDataList.toString());

                                ConstantVal.progressDialog.dismiss();
//                                settingsDialog.dismiss();
//                                Toast.makeText(context, ConstantVal.RESPONSE, Toast.LENGTH_SHORT).show();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

//                                Toast.makeText(SelectionActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(filelist.get(position).getUser_id()));
                params.put("date_of_schedule", date);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 1)
            getMenuInflater().inflate(R.menu.therapist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        ConstantVal.BACK = 1;
        Intent intent = new Intent(this, MapsActivity.class);

        startActivity(intent);
        finish();
    }
}

