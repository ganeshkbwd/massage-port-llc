package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.TherapistListAdapter;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.TherapistData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SelectionActivity extends AppCompatActivity implements View.OnClickListener {
    TherapistListAdapter therapistListAdapter;
    List<TherapistData> therapistDataList = new ArrayList<TherapistData>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        changeStatusBarColor();

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.customerTV:
//                getAllTherapist();
                ConstantVal.USER_TYPE = "Customer";
                intent = new Intent(this, MapsActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.therapistTV:
                ConstantVal.USER_TYPE = "Therapist";
                intent = new Intent(this, TherapistSignInActivity.class);
                startActivity(intent);
                finish();
//                Toast.makeText(SelectionActivity.this, "Under development ...", Toast.LENGTH_SHORT).show();
                break;

        }

    }
}
