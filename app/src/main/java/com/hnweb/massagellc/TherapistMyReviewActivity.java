package com.hnweb.massagellc;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.MyReviewsAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.MyReviews;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class TherapistMyReviewActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id;
    ArrayList<MyReviews> mRList = new ArrayList<MyReviews>();
    ListView myReviewsLV;
    ImageView statusBTN;
    String status1 = "off";
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapist_my_review);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        myReviewsLV = (ListView) findViewById(R.id.myReviewsLV);

        if (CheckConnectivity.checkInternetConnection(TherapistMyReviewActivity.this)) {
            checkStatus();

        } else {
            CheckConnectivity.noNetMeg(TherapistMyReviewActivity.this);
        }

        statusBTN = (ImageView) toolbar.findViewById(R.id.statusIV);
        statusBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckConnectivity.checkInternetConnection(TherapistMyReviewActivity.this)) {
                    callAvailStatusWebservice(TherapistMyReviewActivity.this);
                } else {
                    CheckConnectivity.noNetMeg(TherapistMyReviewActivity.this);
                }

            }
        });

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }


    private void getMyReviews() {
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.therapistMyReviews,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {

                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    MyReviews myReviews = new MyReviews();
                                    myReviews.setReview_id(jsonArray.getJSONObject(i).getInt("review_id"));
                                    myReviews.setCustomer_id(jsonArray.getJSONObject(i).getInt("customer_id"));
                                    myReviews.setTherapist_id(jsonArray.getJSONObject(i).getInt("therapist_id"));
                                    myReviews.setComment(jsonArray.getJSONObject(i).getString("comment"));
                                    myReviews.setStars(jsonArray.getJSONObject(i).getString("stars"));
                                    myReviews.setCreated_datetime(jsonArray.getJSONObject(i).getString("created_datetime"));
                                    myReviews.setReply_text(jsonArray.getJSONObject(i).getString("reply_text"));
                                    myReviews.setCustomer_name(jsonArray.getJSONObject(i).getString("customer_name"));
                                    mRList.add(myReviews);
                                }
//                                customerReviewLV.setAdapter(new CustomerSideReviewsAdapter(ReviewCustomerSideActivity.this, csrList));
                                myReviewsLV.setAdapter(new MyReviewsAdapter(TherapistMyReviewActivity.this, mRList));
                                ConstantVal.progressDialog.dismiss();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistMyReviewActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);
    }


    public void callAvailStatusWebservice(final Activity context) {

        ConstantVal.progressDialog = new ProgressDialog(context);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/massageportllc/api/update_therapist_status.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {

                                if (status1.equalsIgnoreCase("off")) {
                                    status1 = "on";
                                    statusBTN.setImageResource(R.drawable.availible_off_button);

                                } else {
                                    status1 = "off";
                                    statusBTN.setImageResource(R.drawable.availible_on_button);

                                }
                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(context, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 2)
            getMenuInflater().inflate(R.menu.therapist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    public void checkStatus() {
//        getStringImage(bitmap);
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Uploading ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getCurrentStatus,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");


                            if (message_code == 1) {
                                String message = jobj.getString("message");
//                                profileIV.setImageBitmap(photo);
                                ConstantVal.AVAILABLE_STATUS = jobj.getString("available_status");
                                if (ConstantVal.AVAILABLE_STATUS.equalsIgnoreCase("Yes")) {
                                    statusBTN.setImageResource(R.drawable.availible_on_button);
                                } else {
                                    statusBTN.setImageResource(R.drawable.availible_off_button);
                                }
                                ConstantVal.progressDialog.dismiss();
                                getMyReviews();

                            } else {
                                String message = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();

                Toast.makeText(TherapistMyReviewActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }
}
