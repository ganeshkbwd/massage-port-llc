package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.CustomerReviewAdapter;
import com.hnweb.massagellc.adapter.CustomerSideReviewsAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.CustomerSideReviews;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class ReviewCustomerSideActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<CustomerSideReviews> csrList = new ArrayList<CustomerSideReviews>();
    ListView customerReviewLV;
    ImageView therapistProfileIV;
    TextView therapistNameTV;
    RatingBar currentRB, newRB;
    //    String therapistId;
    float myRating;
    EditText reviewET;
    int login, user_id;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    Toolbar toolbar;
    TextView noReviewTV;
    Bundle b;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_customer_side);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        b = getIntent().getExtras();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        customerReviewLV = (ListView) findViewById(R.id.reviwesLV);
        therapistProfileIV = (ImageView) findViewById(R.id.therapist_profileIV);
        therapistNameTV = (TextView) findViewById(R.id.therapistNameTV);
        currentRB = (RatingBar) findViewById(R.id.ratingBarTV);
        ratingBarSet(currentRB);
        newRB = (RatingBar) findViewById(R.id.ratingTV);
        ratingBarSet(newRB);
        reviewET = (EditText) findViewById(R.id.reviewET);
        noReviewTV = (TextView) findViewById(R.id.noReviewTV);


        if (b.getString("ComeFrom").equalsIgnoreCase("SignIn")) {
            reviewET.setText(ConstantVal.reviewMsg);
            newRB.setRating(ConstantVal.myRatings);
            if (CheckConnectivity.checkInternetConnection(ReviewCustomerSideActivity.this)) {
                addReviewByCustomerWebService(ConstantVal.reviewMsg, ConstantVal.myRatings, ConstantVal.THERAPIST_ID_REVIEW, user_id);
            } else {
                CheckConnectivity.noNetMeg(ReviewCustomerSideActivity.this);
            }

//            reviewsCustomerWebService(ConstantVal.THERAPIST_ID_REVIEW);
        } else if (b.getString("ComeFrom").equalsIgnoreCase("Book")) {
            ConstantVal.THERAPIST_ID_REVIEW = b.getString("TherapistID");
            if (CheckConnectivity.checkInternetConnection(ReviewCustomerSideActivity.this)) {
                reviewsCustomerWebService(ConstantVal.THERAPIST_ID_REVIEW);
            } else {
                CheckConnectivity.noNetMeg(ReviewCustomerSideActivity.this);
            }

        }


        newRB.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                myRating = rating;
//                Toast.makeText(ReviewCustomerSideActivity.this, "Rating: " + rating,
//                        Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void ratingBarSet(RatingBar setRB) {
        LayerDrawable stars = (LayerDrawable) setRB.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitBTN:

                if (newRB.getRating() != 0) {
                    String myComments = reviewET.getText().toString();

                    if (login == 1) {
                        int customer_id = user_id;
                        if (CheckConnectivity.checkInternetConnection(ReviewCustomerSideActivity.this)) {
                            addReviewByCustomerWebService(myComments, newRB.getRating(), ConstantVal.THERAPIST_ID_REVIEW, customer_id);
                        } else {
                            CheckConnectivity.noNetMeg(ReviewCustomerSideActivity.this);
                        }

                    } else {
//                        Toast.makeText(ReviewCustomerSideActivity.this, "Under Development...Needs logged in", Toast.LENGTH_SHORT).show();
                        ConstantVal.reviewMsg = reviewET.getText().toString().trim();
                        ConstantVal.myRatings = newRB.getRating();
                        Intent intent = new Intent(ReviewCustomerSideActivity.this, SignInActivity.class);
                        intent.putExtra("ComeFrom", "Review");
                        startActivity(intent);
                    }


                } else {
                    Toast.makeText(this, "Please give rating", Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }

    public void reviewsCustomerWebService(final String therapistId) {


        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getReviewsToShowCustomer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                csrList.clear();
                                noReviewTV.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);

                                String therapist_name = jobj.getString("therapist_name");
                                String therapist_photo = jobj.getString("therapist_photo");
                                String avg_rating = jobj.getString("avg_rating");

                                JSONArray jarr = jobj.getJSONArray("response");

                                for (int i = 0; i < jarr.length(); i++) {
                                    CustomerSideReviews csr = new CustomerSideReviews();
                                    csr.setReview_id(jarr.getJSONObject(i).getInt("review_id"));
                                    csr.setCustomer_id(jarr.getJSONObject(i).getInt("customer_id"));
                                    csr.setTherapist_id(jarr.getJSONObject(i).getInt("therapist_id"));
                                    csr.setComment(jarr.getJSONObject(i).getString("comment"));
                                    csr.setStars(jarr.getJSONObject(i).getString("stars"));
                                    csr.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    csr.setReply_text(jarr.getJSONObject(i).getString("reply_text"));
                                    csr.setCustomer_name(jarr.getJSONObject(i).getString("customer_name"));

                                    csrList.add(csr);
                                }

                                therapistNameTV.setText(therapist_name);
                                Picasso.with(ReviewCustomerSideActivity.this).load(therapist_photo).into(therapistProfileIV);
                                currentRB.setRating(Float.parseFloat(avg_rating));
                                recyclerView.setAdapter(new CustomerReviewAdapter(ReviewCustomerSideActivity.this, csrList));
//                                customerReviewLV.setAdapter(new CustomerSideReviewsAdapter(ReviewCustomerSideActivity.this, csrList));

                                ConstantVal.progressDialog.dismiss();


                            } else {
                                recyclerView.setVisibility(View.INVISIBLE);
                                noReviewTV.setVisibility(View.VISIBLE);
                                String message = jobj.getString("message");
//                                Toast.makeText(ReviewCustomerSideActivity.this, message, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(ReviewCustomerSideActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", therapistId);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void addReviewByCustomerWebService(final String myComments, final float rating, final String therapistId, final int customer_id) {


        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.addReviewsByCustomer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            String message = jobj.getString("message");

                            ConstantVal.progressDialog.dismiss();
                            Toast.makeText(ReviewCustomerSideActivity.this, "You have been submitted your review for this therapist.", Toast.LENGTH_SHORT).show();
                            reviewsCustomerWebService(therapistId);
                            newRB.setRating(0);
                            reviewET.setText("");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(ReviewCustomerSideActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("customer_id", String.valueOf(customer_id));
                params.put("therapist_id", therapistId);
                params.put("comment", myComments);
                params.put("stars", String.valueOf(rating));

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 1)
            getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (b.getString("ComeFrom").equalsIgnoreCase("SignIn")) {
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
            finish();
        } else if (b.getString("ComeFrom").equalsIgnoreCase("Book")) {
            Intent intent = new Intent(this, TherapistProfileActivity.class);
            intent.putExtra("ComeFrom", "Review");
            startActivity(intent);
            finish();
        }

//        super.finish();
    }
}
