package com.hnweb.massagellc.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.adapter.CustomersAdapter;
import com.hnweb.massagellc.adapter.TherapisttoTherapistAdapter;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.Customers;
import com.hnweb.massagellc.pojo.TherapistToTherapist;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by neha on 11/11/2016.
 */
public class MessagesFragment extends Fragment implements View.OnClickListener {
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id;
    ArrayList<Customers> customersArrayList = new ArrayList<Customers>();
    ArrayList<TherapistToTherapist> therapistArrayList = new ArrayList<TherapistToTherapist>();

    RecyclerView recyclerView, recycler_view1;
    Button customerListBTN, therapistsListBTN;
    TextView customerEnabledTV, therapistEnabledTV;

    public MessagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            scheduledAppointmentWebService();
            customerEnabledTV.setBackgroundColor(Color.WHITE);
            therapistEnabledTV.setBackgroundColor(getResources().getColor(R.color.textColor));
            customerListBTN.setEnabled(false);
            therapistsListBTN.setEnabled(true);
            messageWebservice();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.message_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        customerListBTN = (Button) v.findViewById(R.id.customerListBTN);
        therapistsListBTN = (Button) v.findViewById(R.id.therapistsListBTN);
        therapistEnabledTV = (TextView) v.findViewById(R.id.therapistEnabledTV);
        customerEnabledTV = (TextView) v.findViewById(R.id.customerEnabledTV);
        therapistsListBTN.setOnClickListener(this);
        customerListBTN.setOnClickListener(this);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recycler_view1 = (RecyclerView) v.findViewById(R.id.recycler_view1);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler_view1.setLayoutManager(new LinearLayoutManager(getActivity()));
//        messageWebservice();
        return v;

    }


    public void messageTherapistWebservice() {
        try {
            ConstantVal.progressDialog = new ProgressDialog(getActivity());
            ConstantVal.progressDialog.setMessage("Please wait ....");
            ConstantVal.progressDialog.show();
        } catch (Exception e) {
//            ConstantVal.progressDialog = new ProgressDialog(getActivity().getParent());
//            ConstantVal.progressDialog.setMessage("Please wait ....");
//            ConstantVal.progressDialog.show();
        }


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getTherapistForTherapist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

//                            Log.e()
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");


                            if (ConstantVal.MESSAGE_CODE == 1) {
//

                                therapistArrayList.clear();

                                JSONArray jarr = jobj.getJSONArray("response");
                                for (int i = 0; i < jarr.length(); i++) {

                                    TherapistToTherapist c = new TherapistToTherapist();
                                    c.setTherapist_name(jarr.getJSONObject(i).getString("therapist_name"));
                                    c.setTo_user_id(jarr.getJSONObject(i).getString("to_user_id"));
                                    c.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));


                                    therapistArrayList.add(c);


                                }
                                recycler_view1.setAdapter(new TherapisttoTherapistAdapter(getActivity(), therapistArrayList));

                                ConstantVal.progressDialog.dismiss();


                            } else {
                                ConstantVal.MESSAGE = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("loggedin_therapist_id", String.valueOf(39));


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void messageWebservice() {
        try {
            ConstantVal.progressDialog = new ProgressDialog(getActivity());
            ConstantVal.progressDialog.setMessage("Please wait ....");
            ConstantVal.progressDialog.show();
        } catch (Exception e) {
//            ConstantVal.progressDialog = new ProgressDialog(getActivity().getParent());
//            ConstantVal.progressDialog.setMessage("Please wait ....");
//            ConstantVal.progressDialog.show();
        }


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getCustomers,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");


                            if (ConstantVal.MESSAGE_CODE == 1) {
//

                                customersArrayList.clear();

                                JSONArray jarr = jobj.getJSONArray("response");
                                for (int i = 0; i < jarr.length(); i++) {

                                    Customers c = new Customers();
                                    c.setBooking_id(jarr.getJSONObject(i).getInt("booking_id"));
                                    c.setBooking_type(jarr.getJSONObject(i).getString("booking_type"));
                                    c.setDate(jarr.getJSONObject(i).getString("date"));
                                    c.setHour(jarr.getJSONObject(i).getString("hour"));
                                    c.setMinute(jarr.getJSONObject(i).getString("minute"));
                                    c.setPeriod(jarr.getJSONObject(i).getString("period"));
                                    c.setCustomer_id(jarr.getJSONObject(i).getString("customer_id"));
                                    c.setTherapist_id(jarr.getJSONObject(i).getString("therapist_id"));
                                    c.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    c.setCustomer_address(jarr.getJSONObject(i).getString("customer_address"));
                                    c.setCustomer_name(jarr.getJSONObject(i).getString("customer_name"));
                                    c.setFull_address(jarr.getJSONObject(i).getString("full_address"));

                                    customersArrayList.add(c);


                                }
                                recyclerView.setAdapter(new CustomersAdapter(getActivity(), customersArrayList));

                                ConstantVal.progressDialog.dismiss();


                            } else {
                                ConstantVal.MESSAGE = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.customerListBTN:
                customerEnabledTV.setBackgroundColor(Color.WHITE);
                therapistEnabledTV.setBackgroundColor(getResources().getColor(R.color.textColor));
                customerListBTN.setEnabled(false);
//                customerListBTN.setTextColor(getResources().getColor(R.color.textColor));
                therapistsListBTN.setEnabled(true);
                recyclerView.setVisibility(View.VISIBLE);
                recycler_view1.setVisibility(View.GONE);
                messageWebservice();
                break;
            case R.id.therapistsListBTN:
                customerEnabledTV.setBackgroundColor(getResources().getColor(R.color.textColor));
                therapistEnabledTV.setBackgroundColor(Color.WHITE);
                customerListBTN.setEnabled(true);
                therapistsListBTN.setEnabled(false);
                recycler_view1.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                messageTherapistWebservice();
                break;
        }
    }
}

