package com.hnweb.massagellc.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.adapter.TherapistAppointmentsAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.TherapistConfirmAppointments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by neha on 10/8/2016.
 */
public class MyAppointmentFragment extends Fragment {
    ArrayList<TherapistConfirmAppointments> msaList = new ArrayList<TherapistConfirmAppointments>();
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id;
    ListView scheduleAppLV;
    TextView noAppointmentsTV;


    public MyAppointmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if (CheckConnectivity.checkInternetConnection(getActivity())) {
                scheduledAppointmentWebService();
            } else {
                CheckConnectivity.noNetMeg(getActivity());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.myappointment_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);

        init(v);



        return v;
    }

    public void init(View v) {
        scheduleAppLV = (ListView) v.findViewById(R.id.scheduleAppLV);
        noAppointmentsTV = (TextView) v.findViewById(R.id.noReviewTV);

    }

    public void scheduledAppointmentWebService() {


        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.therapistAppDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                scheduleAppLV.setVisibility(View.VISIBLE);
                                noAppointmentsTV.setVisibility(View.GONE);
                                msaList.clear();
                                JSONArray jarr = jobj.getJSONArray("response");
                                for (int i = 0; i < jarr.length(); i++) {
                                    TherapistConfirmAppointments msa = new TherapistConfirmAppointments();
                                    msa.setBooking_id(jarr.getJSONObject(i).getInt("booking_id"));
                                    msa.setBooking_type(jarr.getJSONObject(i).getString("booking_type"));
                                    msa.setDate(jarr.getJSONObject(i).getString("date"));
                                    msa.setHour(jarr.getJSONObject(i).getString("hour"));
                                    msa.setMinute(jarr.getJSONObject(i).getString("minute"));
                                    msa.setPeriod(jarr.getJSONObject(i).getString("period"));
                                    msa.setCustomer_id(jarr.getJSONObject(i).getInt("customer_id"));
                                    msa.setTherapist_id(jarr.getJSONObject(i).getInt("therapist_id"));
                                    msa.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    msa.setCustomer_address(jarr.getJSONObject(i).getString("customer_address"));
                                    msa.setCustomerName(jarr.getJSONObject(i).getString("customer_name"));
                                    Log.e("NAME",jarr.getJSONObject(i).getString("customer_name"));
                                    msa.setFull_address(jarr.getJSONObject(i).getString("full_address"));


                                    msaList.add(msa);


                                }
                                scheduleAppLV.setAdapter(new TherapistAppointmentsAdapter(getActivity(), msaList));

//                                Toast.makeText(MyAppointmentActivity.this, "Your appointment booked successfully.", Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();
//                                Intent intent = new Intent(MyAppointmentActivity.this, MapsActivity.class);
//                                startActivity(intent);
//                                finish();

                            } else {
                                scheduleAppLV.setVisibility(View.GONE);
                                noAppointmentsTV.setVisibility(View.VISIBLE);
                                ConstantVal.MESSAGE = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MyAppointmentActivity.this,ConstantVal.MESSAGE_CODE,Toast.LENGTH_SHORT).show();

                            }
                            ConstantVal.progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }
}
