package com.hnweb.massagellc.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.TherapistMyProfileActivity;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.AllTherapists;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by neha on 10/8/2016.
 */
public class ViewMapFragment extends Fragment implements GoogleMap.OnMarkerClickListener {
    MapView mMapView;
    private GoogleMap googleMap;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id;
    String registerdLong, registerdLat;
    ArrayList<AllTherapists> atList = new ArrayList<AllTherapists>();
    Geocoder geocoder;
    List<Address> addresses;
    String address = "", city = "", state, country = "", postalCode, subLocality;
    int isCREATED = 0;
private  ProgressDialog progressDialog;

    public ViewMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

//        @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            onResume();
//
//        }
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait ....");
        View rootView = inflater.inflate(R.layout.therapist_map_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        registerdLat = sharedPreferences.getString("registerdLat", null);
        registerdLong = sharedPreferences.getString("registerdLong", null);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        setUpMap();

        return rootView;
    }

    public void setUpMap() {
//        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(registerdLat),
                                Double.parseDouble(registerdLong)));
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(7);

                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);
//                googleMap.addMarker(new MarkerOptions().position(
//                        new LatLng(Methodes.myLocation(getActivity()).getLatitude(), Methodes.myLocation(getActivity()).getLongitude())).draggable(true))
//                ;
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        int position = atList.get(Integer.parseInt(marker.getId().substring(1))).getUser_id();
                        if (user_id == position) {

                            Bundle b = new Bundle();
                            b.putInt("Position", Integer.parseInt(marker.getId().substring(1)));
                            b.putSerializable("ALLTherapist", atList);
                            Intent i = new Intent(getActivity(), TherapistMyProfileActivity.class);
                            i.putExtras(b);
                            getActivity().startActivity(i);

                            Log.e("MARKED ID", marker.getId());
                        } else {

                            Bundle b = new Bundle();
                            b.putInt("Position", Integer.parseInt(marker.getId().substring(1)));
                            b.putSerializable("ALLTherapist", atList);
                            Intent i = new Intent(getActivity(), TherapistMyProfileActivity.class);
                            i.putExtras(b);
                            getActivity().startActivity(i);

                            Log.e("MARKED ID FALSE", marker.getId());
//                            Toast.makeText(getActivity(), "Not you.", Toast.LENGTH_SHORT).show();
                        }

                        return true;
                    }
                });
                googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {


                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {

                        LatLng dragPosition = marker.getPosition();
                        double dragLat = dragPosition.latitude;
                        double dragLong = dragPosition.longitude;
                        Log.i("info", "on drag end :" + dragLat + " dragLong :" + dragLong);
                        marker.setPosition(new LatLng(dragLat, dragLong));
//                        Toast.makeText(getActivity().getApplicationContext(), "Marker Dragged..!" + dragLat + "," + dragLong, Toast.LENGTH_LONG).show();
                        String full_address = getAddressFromLatLong(dragLat, dragLong);
                        if (CheckConnectivity.checkInternetConnection(getActivity())) {
                            changeTherapistLocation(dragLat, dragLong, full_address);
                        } else {
                            CheckConnectivity.noNetMeg(getActivity());
                        }


                    }
                });
                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {

//                        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//                        googleMap.addMarker(new MarkerOptions()
//                                .position(latLng)
//                                .draggable(true));
                    }
                });
                googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        //create new marker when user long clicks
//                        googleMap.addMarker(new MarkerOptions()
//                                .position(latLng)
//                                .draggable(true));
                    }
                });
                getAllTherapist(Double.parseDouble(registerdLat), Double.parseDouble(registerdLong));

                // For showing a move to my location button
//                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
//                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
//                LatLng sydney = new LatLng(Methodes.myLocation(getActivity()).getLatitude(), Methodes.myLocation(getActivity()).getLongitude());
//                googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));
//
//                // For zooming automatically to the location of the marker
//                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
//                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        setUpMap();
        mMapView.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        ////        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        mMapView.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }




    public void getAllTherapist(final double lattitude, final double longitude) {

        progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/massageportllc/api/list_available_therapists.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                atList.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    AllTherapists std = new AllTherapists();
                                    std.setUser_id(jsonArray.getJSONObject(i).getInt("user_id"));
                                    std.setAvailable_status(jsonArray.getJSONObject(i).getString("available_status"));
                                    std.setFull_name(jsonArray.getJSONObject(i).getString("full_name"));
                                    std.setEmail_address(jsonArray.getJSONObject(i).getString("email_address"));
                                    std.setUsername(jsonArray.getJSONObject(i).getString("username"));
                                    std.setLatitude(jsonArray.getJSONObject(i).getString("latitude"));
                                    std.setLongitude(jsonArray.getJSONObject(i).getString("longitude"));
                                    std.setFull_address(jsonArray.getJSONObject(i).getString("full_address"));
                                    std.setExpired_datetime(jsonArray.getJSONObject(i).getString("expired_datetime"));
                                    std.setProfile_photo(jsonArray.getJSONObject(i).getString("profile_photo"));
                                    std.setSpeciality(jsonArray.getJSONObject(i).getString("speciality"));
                                    std.setRates(jsonArray.getJSONObject(i).getString("rates"));
                                    std.setAvg_rating(jsonArray.getJSONObject(i).getString("avg_rating"));
                                    std.setTotal_reviews(jsonArray.getJSONObject(i).getString("total_reviews"));
                                    atList.add(std);
//
                                }
                                if (atList != null) {
                                    for (int i = 0; i < atList.size(); i++) {
                                        double lati = Double.parseDouble(atList.get(i).getLatitude());
                                        double longLat = Double.parseDouble(atList.get(i).getLongitude());
                                        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
//                                        ImageView markerProfile = (ImageView) markerView.findViewById(R.id.profileIV);
//                                        Picasso.with(MapsActivity.this).load(showTherapistData.get(i).getProfile_photo().toString()).into(markerProfile);
//                                        markerView.setDrawingCacheEnabled(true);
//                                        markerView.buildDrawingCache();
////
//                                        bm = markerView.getDrawingCache();
                                        TextView statusTV = (TextView) marker.findViewById(R.id.statusTV);
                                        if (atList.get(i).getAvailable_status().equalsIgnoreCase("Yes")) {
                                            statusTV.setText("Available Now");
                                        } else {
                                            statusTV.setText("Not Available");
                                        }

                                        ImageView profileIv = (ImageView) marker.findViewById(R.id.profileIV);
                                        if (!atList.get(i).getProfile_photo().toString().equalsIgnoreCase("")) {
                                            Picasso.with(getActivity()).load(atList.get(i).getProfile_photo().toString()).into(profileIv);
                                        }
                                        RatingBar ratingBar = (RatingBar) marker.findViewById(R.id.ratingBar);
                                        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
                                        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                                        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                                        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                                        ratingBar.setRating(Float.parseFloat(atList.get(i).getAvg_rating()));
                                        TextView reviewTv = (TextView) marker.findViewById(R.id.reviewsTV);
                                        reviewTv.setText("(" + atList.get(i).getTotal_reviews().toString() + ")");

                                        if (atList.get(i).getUser_id() == user_id) {
                                            googleMap.addMarker(new MarkerOptions().position(
                                                    new LatLng(lati, longLat)).draggable(true)
                                                    .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))))
                                            ;
                                        } else {
                                            googleMap.addMarker(new MarkerOptions().position(
                                                    new LatLng(lati, longLat))
                                                    .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))))
                                            ;
                                        }


//                                        googleMap.setOnMarkerClickListener(getActivity());
                                    }
//                                    ConstantVal.progressDialog.dismiss();
                                }
//                                ConstantVal.progressDialog.dismiss();
//
                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message");
//                                ConstantVal.progressDialog.dismiss();//
//                                Toast.makeText(MapsActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (progressDialog!= null&& progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog!= null&& progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
//                params.put("latitude", String.valueOf(lattitude));
//                params.put("longitude", String.valueOf(longitude));
                //returning parameters
                return params;
            }
        };
        int socketTimeout = 5000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        queue.add(stringRequest);
    }

    public void changeTherapistLocation(final double lattitude, final double longitude, final String full_address) {
        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.updateTherapistLocation,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {

                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putString("registerdLat", String.valueOf(lattitude));
                                editor.putString("registerdLong", String.valueOf(longitude));
                                editor.commit();
                                ConstantVal.progressDialog.dismiss();
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            } else {

                                ConstantVal.progressDialog.dismiss();
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }

                            ConstantVal.progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            ConstantVal.progressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("latitude", String.valueOf(lattitude));
                params.put("longitude", String.valueOf(longitude));
                params.put("full_address", full_address);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
//        marker.getId();
        Log.e("MARKER ID", marker.getId());
        return true;
    }

    public String getAddressFromLatLong(double lattitude, double longitude) {
        String fulladdress = "";
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lattitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            subLocality = addresses.get(0).getSubLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        fulladdress = address + "," + subLocality + "," + city + "," + state + "," + country;
        return fulladdress;
    }
}
