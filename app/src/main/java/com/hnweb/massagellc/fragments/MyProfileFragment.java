package com.hnweb.massagellc.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.SplashActivity;
import com.hnweb.massagellc.adapter.SetAppointmentTimingsAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.ScalingUtilities;
import com.hnweb.massagellc.helper.Urls;
import com.squareup.picasso.Picasso;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

/**
 * Created by neha on 10/8/2016.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener {
    TextView editProfile, updateStatus;
    LinearLayout therapistProfileLayout, calenderLayout;
    View viewSmall, viewSmall1;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id;
    int therapist_id;
    String full_name, email_address, username, password, profile_photo, speciality, rates, available_status, total_reviews;
    ImageView profileIV;
    EditText nameET, userNameET, passwordET, confirmPasswordET, email_idET, specialityET, ratesET;
    ArrayList<String> distinctAvailableDatesList = new ArrayList<String>();
    CustomCalendarView calendarView;
    private static final int SELECT_FILE = 11;
    private static final int REQUEST_CAMERA = 12;
    private String encodedImage = "";
    public static int AVAIL = 0, IMAGESET = 0;
    private Bitmap profileImage;
    Button uploadBTN, saveBTN, updateStatusBTN;
//    ArrayList<String> selectedTime = new ArrayList<String>();
    //    ArrayList<AvailableTimings> availableTimings = new ArrayList<AvailableTimings>();


    public MyProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (CheckConnectivity.checkInternetConnection(getActivity())) {
                        therapistDetailsWebService();
                    } else {
                        CheckConnectivity.noNetMeg(getActivity());
                    }

                }
            });


        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.profile_fragment, container, false);
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);


        init(v);

        editProfile.setOnClickListener(listener);
        updateStatus.setOnClickListener(listener);

        return v;
    }

    public void init(View v) {

        editProfile = (TextView) v.findViewById(R.id.epET);
        updateStatus = (TextView) v.findViewById(R.id.usET);
        therapistProfileLayout = (LinearLayout) v.findViewById(R.id.therapistProfileLayout);
        calenderLayout = (LinearLayout) v.findViewById(R.id.calenderLayout);
        viewSmall = (View) v.findViewById(R.id.epV);
        viewSmall1 = (View) v.findViewById(R.id.usV);

        profileIV = (ImageView) v.findViewById(R.id.profileIV);
        nameET = (EditText) v.findViewById(R.id.nameET);
        userNameET = (EditText) v.findViewById(R.id.userNameET);
        passwordET = (EditText) v.findViewById(R.id.passET);
        confirmPasswordET = (EditText) v.findViewById(R.id.confirmPassET);
        email_idET = (EditText) v.findViewById(R.id.emailET);
        specialityET = (EditText) v.findViewById(R.id.specialityET);
        ratesET = (EditText) v.findViewById(R.id.ratesET);
        uploadBTN = (Button) v.findViewById(R.id.uploadBTN);
        saveBTN = (Button) v.findViewById(R.id.saveBTN);
        updateStatusBTN = (Button) v.findViewById(R.id.updateStatusBTN);

        uploadBTN.setOnClickListener(this);
        saveBTN.setOnClickListener(this);
        updateStatusBTN.setOnClickListener(this);


        calendarView = (CustomCalendarView) v.findViewById(R.id.calendar_view);
        setCalenderView();
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.epET:
                    editProfile.setTextColor(getResources().getColor(R.color.textColor));
                    updateStatus.setTextColor(getResources().getColor(R.color.white));
                    therapistProfileLayout.setVisibility(View.VISIBLE);
                    calenderLayout.setVisibility(View.GONE);
                    viewSmall.setElevation(10);
                    viewSmall1.setElevation(0);
                    viewSmall.setBackgroundColor(getResources().getColor(R.color.textColor));
                    viewSmall1.setBackgroundColor(getResources().getColor(R.color.white));
                    if (CheckConnectivity.checkInternetConnection(getActivity())) {
                        therapistDetailsWebService();
                    } else {
                        CheckConnectivity.noNetMeg(getActivity());
                    }

                    break;
                case R.id.usET:
                    if (CheckConnectivity.checkInternetConnection(getActivity())) {
                        getDistinctAvailableDates();
                    } else {
                        CheckConnectivity.noNetMeg(getActivity());
                    }

                    therapistProfileLayout.setVisibility(View.GONE);
                    calenderLayout.setVisibility(View.VISIBLE);
                    editProfile.setTextColor(getResources().getColor(R.color.white));
                    updateStatus.setTextColor(getResources().getColor(R.color.textColor));
                    viewSmall1.setElevation(10);
                    viewSmall.setElevation(0);
                    viewSmall1.setBackgroundColor(getResources().getColor(R.color.textColor));
                    viewSmall.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
            }
        }
    };

    public void therapistDetailsWebService() {


        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.therapistDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {

                                ConstantVal.THERAPIST_PROFILE = "";
                                therapist_id = jobj.getInt("user_id");
                                full_name = jobj.getString("full_name");
                                email_address = jobj.getString("email_address");
                                username = jobj.getString("username");
                                password = jobj.getString("password");
                                profile_photo = jobj.getString("profile_photo");
                                speciality = jobj.getString("speciality");
                                rates = jobj.getString("rates");
                                available_status = jobj.getString("available_status");
                                ConstantVal.TOTAL_REVIEWS = jobj.getInt("total_reviews");

                                nameET.setText(full_name);
                                passwordET.setText(password);
                                email_idET.setText(email_address);
                                confirmPasswordET.setText(password);
                                userNameET.setText(username);
                                nameET.setError(null);
                                passwordET.setError(null);
                                email_idET.setError(null);
                                confirmPasswordET.setError(null);
                                userNameET.setError(null);
                                if (!profile_photo.equals("")) {
                                    Picasso.with(getActivity()).load(profile_photo).into(profileIV);
                                }
                                ConstantVal.THERAPIST_PROFILE = profile_photo;
                                specialityET.setText(speciality);
                                ratesET.setText(rates);
                                specialityET.setError(null);
                                ratesET.setError(null);


                                ConstantVal.progressDialog.dismiss();
                            } else {
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    private void getDistinctAvailableDates() {
        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getDistinctAvailableDates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                distinctAvailableDatesList.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String distinct_date = jsonArray.getJSONObject(i).getString("date");
                                    distinctAvailableDatesList.add(distinct_date);

                                }
                                calendarView.setSelectedDayGreen(distinctAvailableDatesList);

                                ConstantVal.progressDialog.dismiss();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);
    }

    public void setCalenderView() {
        //Initialize calendar with date
        final Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);

        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();

                if (!Methodes.isPastDay(date)) {

                    ConstantVal.date_selected = df.format(date);

                    if (distinctAvailableDatesList.contains(ConstantVal.date_selected)) {
                        callweservicesToBook(df.format(date), date);
                    } else {
                        ConstantVal.avialAllreadyTimeList.clear();
                        setAppointmentTimeDialog(date);
                    }

                } else {
                    Toast.makeText(getActivity(), "You can not set schedule for past date", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onMonthChanged(Date date) {

//                calendarView.refreshCalendar(currentCalendar);
                DateFormat dateFormat = new SimpleDateFormat("MM");
                Date date1 = new Date();
                Log.e("Month", dateFormat.format(date1));
//
                int monthChanged = Integer.parseInt(dateFormat.format(date1));
                int monthCurrent = Integer.parseInt(dateFormat.format(date));
                if (monthChanged == monthCurrent)
                    calendarView.setSelectedDayGreen(distinctAvailableDatesList);
                else
                    calendarView.setSelectedDayRed(monthCurrent);


//                Toast.makeText(TherapistProfileActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void agreeDialog(final String selectedDate, final String slecTime) {

        final Dialog settingsDialog = new Dialog(getActivity());
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(getActivity().getLayoutInflater().inflate(R.layout.agree_dialog
                , null));
        settingsDialog.setCancelable(false);

        final CheckBox checkBox = (CheckBox) settingsDialog.findViewById(R.id.checkBox2);
        Button agreeBTN = (Button) settingsDialog.findViewById(R.id.agreeBTN);

        agreeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    if (CheckConnectivity.checkInternetConnection(getActivity())) {
                        callSetScheduleWebservice(selectedDate, slecTime, settingsDialog);
                    } else {
                        CheckConnectivity.noNetMeg(getActivity());
                    }
                } else {
                    Toast.makeText(getActivity(),
                            "To proceed further please agree terms & conditions.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        settingsDialog.show();

    }

    public void setAppointmentTimeDialog(Date date) {


        final Dialog settingsDialog = new Dialog(getActivity());
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(getActivity().getLayoutInflater().inflate(R.layout.set_appointment_time_dialog
                , null));
        settingsDialog.setCancelable(false);
        final ListView timeLV = (ListView) settingsDialog.findViewById(R.id.setTimeLV);
        Button bookBTN = (Button) settingsDialog.findViewById(R.id.setTimeBTN);
        Button cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);
        ConstantVal.selectedPosition = -1;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String selectedDate = dateFormat.format(date);

//        if (ConstantVal.avialAllreadyTimeList.size()!=0){
//            selectedTime = ConstantVal.avialAllreadyTimeList;
//        }


        timeLV.setAdapter(new SetAppointmentTimingsAdapter(SplashActivity.setAppointmentTimeList, getActivity()));
        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });

        bookBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String slec = "";
                if (ConstantVal.avialAllreadyTimeList.size() > 0) {
                    for (String s : ConstantVal.avialAllreadyTimeList) {
                        slec += s + "$$";
                    }
                    slec = slec.replaceAll(" ", ":");
                    slec = slec.substring(0, slec.length() - 2);
                    Log.e("Checked Count", slec);
                    if (ConstantVal.TOTAL_REVIEWS == 0) {
                        settingsDialog.dismiss();
                        agreeDialog(selectedDate, slec);
                    } else {
                        if (CheckConnectivity.checkInternetConnection(getActivity())) {
                            callSetScheduleWebservice(selectedDate, slec, settingsDialog);
                        } else {
                            CheckConnectivity.checkInternetConnection(getActivity());
                        }

                    }
                } else {
                    Toast.makeText(getActivity(), "Please select time.", Toast.LENGTH_SHORT).show();
                }
            }


        });


        settingsDialog.show();


    }

    public void updateTherapistProfile() {


        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.updateTherapistProfile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {

//                                therapistDetailsWebService();
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();


                                ConstantVal.progressDialog.dismiss();
                            } else {
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                String image;
                if (encodedImage.equals("")) {
                    image = "";
                } else {
                    image = encodedImage;
                }


                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("full_name", nameET.getText().toString().trim());
                params.put("email_address", email_idET.getText().toString().trim());
                params.put("username", userNameET.getText().toString().trim());
                params.put("password", passwordET.getText().toString().trim());
                params.put("profile_photo", image);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    ///////////////------- Photo Upload -----------///////////
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        BitmapFactory.Options options = new BitmapFactory.Options();

        Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);

        Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bm, 150, 150, ScalingUtilities.ScalingLogic.CROP);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        profileImage = scaledBitmap;
        profileIV.setImageBitmap(scaledBitmap);
        IMAGESET = 1;

    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        int maxHeight = 150;
        int maxWidth = 150;
        float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);

        profileImage = scaledBitmap;
        profileIV.setImageBitmap(scaledBitmap);
        IMAGESET = 1;
//        imageUpload();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {

                onSelectFromGalleryResult(data);

            } else if (requestCode == REQUEST_CAMERA) {

                onCaptureImageResult(data);

            }

        }
    }

    //--------- Profile Photo not Changed ----------//
    public String getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Bitmap thumbnail = BitmapFactory.decodeStream(input);

            int maxHeight = 150;
            int maxWidth = 150;
            float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));

            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
            byte[] byteArrayImage = stream.toByteArray();
            encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);

            profileImage = scaledBitmap;
            profileIV.setImageBitmap(scaledBitmap);
            IMAGESET = 1;


            return encodedImage;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    ////////////------- End Photo Upload ---------////////////

    public void imageUpload(final String speciality, final String rates) {
//        getStringImage(bitmap);
        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Updating ....");
        ConstantVal.progressDialog.show();


//        String uploadurl = "http://designer321.com/johnilbwd/massageportllc/api/update_therapist.php";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.updateTherapistStatus,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
                            String message = jobj.getString("message");
//                            String message = jobj.getString("message");

                            if (message_code == 1) {

//                                profileIV.setImageBitmap(photo);
//                                SharedPreferences.Editor editor = sharedPreferences.edit();
//                                editor.putInt("upload", 1);
//                                editor.commit();
//                                ConstantVal.progressDialog.dismiss();


//                                Intent intent = new Intent(getActivity(), TherapistTabsActivity.class);
//                                startActivity(intent);
//                                finish();
//                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Status Updated successfully", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();

                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();

                params.put("user_id", String.valueOf(user_id));
                params.put("speciality", speciality);
                params.put("rates", rates);
//                params.put("profile_photo", "");

                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }


    public void callSetScheduleWebservice(final String selectedDate, final String slecTime, final Dialog settingsDialog) {

        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();
        ConstantVal.progressDialog.setCancelable(false);


        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/massageportllc/api/set_schedule_of_therapist.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {

                                AVAIL = 1;
                                distinctAvailableDatesList.add(selectedDate);

                                String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                                Log.i("Current Date is", date);
                                calendarView.setNewDateColor(distinctAvailableDatesList, date);

//                                calendarView.setSelectedDayGreen(distinctAvailableDatesList);
                                ConstantVal.progressDialog.dismiss();
                                settingsDialog.dismiss();
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(getActivity(), ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("times", slecTime);
                params.put("date", selectedDate);
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    public void callweservicesToBook(final String date, final Date date1) {
        ConstantVal.progressDialog = new ProgressDialog(getActivity());
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();
        ConstantVal.progressDialog.setCancelable(false);

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getAvailableTimings,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                ConstantVal.avialAllreadyTimeList.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {

//                                    jsonArray.getJSONObject(i).getInt("time_id");
//                                    jsonArray.getJSONObject(i).getInt("therapist_id");
                                    String hour = String.valueOf(jsonArray.getJSONObject(i).getInt("hour"));
                                    String minute = String.valueOf(jsonArray.getJSONObject(i).getInt("minute"));
                                    String period = jsonArray.getJSONObject(i).getString("period");
//                                    jsonArray.getJSONObject(i).getString("date");
//                                    jsonArray.getJSONObject(i).getString("created_date");
//                                    jsonArray.getJSONObject(i).getString("last_updated_date");
//                                    jsonArray.getJSONObject(i).getString("status");

                                    if (Integer.parseInt(hour) < 10) {
                                        hour = "0" + hour;
                                    }
                                    if (Integer.parseInt(minute) < 10) {
                                        minute = "0" + minute;
                                    }
                                    String time = hour + ":" + minute + " " + period;
                                    ConstantVal.avialAllreadyTimeList.add(time);

                                }
                                ConstantVal.progressDialog.dismiss();

                                setAppointmentTimeDialog(date1);
//                                settingsDialog.dismiss();
//                                Toast.makeText(context, ConstantVal.RESPONSE, Toast.LENGTH_SHORT).show();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(getActivity(), "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                params.put("date_of_schedule", date);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.uploadBTN:
                selectImage();
                break;
            case R.id.saveBTN:
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (nameET.getText().toString().trim().length() > 0) {
                            if (userNameET.getText().toString().trim().length() > 0) {
                                if (email_idET.getText().toString().trim().length() > 0
                                        && Patterns.EMAIL_ADDRESS.matcher(email_idET.getText().toString().trim()).matches()) {
                                    if (passwordET.getText().toString().trim().length() > 0) {
                                        if (confirmPasswordET.getText().toString().trim().length() > 0 &&
                                                passwordET.getText().toString().trim().equals(confirmPasswordET.getText().toString().trim())) {
                                            if (CheckConnectivity.checkInternetConnection(getActivity())) {

                                                updateTherapistProfile();
                                            } else {
                                                CheckConnectivity.noNetMeg(getActivity());
                                            }
                                        } else {
                                            confirmPasswordET.setError("Confirm password and password not same, please enter same.");
                                        }
                                    } else {
                                        passwordET.setError("Please enter password.");
                                    }
                                } else {
                                    email_idET.setError("Please enter email id.");
                                }
                            } else {
                                userNameET.setError("Please enter username.");
                            }
                        } else {
                            nameET.setError("Please enter name.");
                        }
                    }
                });


//                else if (saveBTN.getText().toString().trim().equalsIgnoreCase("Edit Profile")) {
//                    saveBTN.setText("Save");
//                    uploadBTN.setEnabled(true);
//                    nameET.setEnabled(true);
//                    nameET.setSelection(nameET.getText().length());
//                    passwordET.setEnabled(true);
//                    confirmPasswordET.setEnabled(true);
//                }
                break;

            case R.id.updateStatusBTN:
                if (specialityET.getText().toString().trim().length() > 2 && ratesET.getText().toString().trim().length() > 0) {
                    imageUpload(specialityET.getText().toString().trim(), ratesET.getText().toString().trim());
                } else {
                    if (specialityET.getText().toString().trim().length() < 3) {
                        specialityET.setError("Please enter speciality.");
                    } else if (ratesET.getText().toString().trim().length() < 3) {
                        ratesET.setError("Please enter rates.");
                    }
                }
                break;
        }
    }
}
