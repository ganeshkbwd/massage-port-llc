package com.hnweb.massagellc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.SetAppointmentTimingsAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.ScalingUtilities;
import com.hnweb.massagellc.helper.Urls;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class TherapistUploadProfileActivity extends AppCompatActivity implements View.OnClickListener {
    CustomCalendarView calendarView;
    Toolbar toolbar;
    private static final int SELECT_FILE = 11;
    private static final int REQUEST_CAMERA = 12;
    private String encodedImage;
    private Bitmap profileImage;
    ImageView profileIV;
    //    ArrayList<String> setAppointmentTimeList;
    ArrayList<String> selectedTime = new ArrayList<String>();
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id, upload;
    EditText specialityET, ratesET;
    Button doneBTN;
    public static int AVAIL = 0, IMAGESET = 0;
    ImageView statusBTN, logoIV;
//    String status = "Yes";
    ArrayList<String> distinctAvailableDatesList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapist_upload_profile);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);


        init();
        checkStatus();
    }

    public void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        logoIV = (ImageView) toolbar.findViewById(R.id.logoIV);
//        RelativeLayout.LayoutParams layoutParams =
//                (RelativeLayout.LayoutParams)toolbar.getLayoutParams();
//        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//        toolbar.setLayoutParams(layoutParams);
        statusBTN = (ImageView) toolbar.findViewById(R.id.statusIV);

        statusBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAvailStatusWebservice();

            }
        });
        setCalenderView();
        profileIV = (ImageView) findViewById(R.id.profileIV);
        specialityET = (EditText) findViewById(R.id.experienceET);
        ratesET = (EditText) findViewById(R.id.ratesET);
        doneBTN = (Button) findViewById(R.id.doneBTN);
//        addTimeSlots();

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void setCalenderView() {
        //Initialize CustomCalendarView from layout
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);

        //Initialize calendar with date
        Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);
        calendarView.setSelectedDayRed(10);
        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                if(!Methodes.isPastDay(date)){

//                Toast.makeText(TherapistUploadProfileActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
                    if (CheckConnectivity.checkInternetConnection(TherapistUploadProfileActivity.this)){
                        setAppointmentTimeDialog(date);
                    }else {
                        CheckConnectivity.noNetMeg(TherapistUploadProfileActivity.this);
                    }
                }else {
                    Toast.makeText(TherapistUploadProfileActivity.this, "You can not set schedule for past date", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onMonthChanged(Date date) {
//                SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
//                Toast.makeText(TherapistUploadProfileActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
                DateFormat dateFormat = new SimpleDateFormat("MM");
                Date date1 = new Date();
                Log.e("Month", dateFormat.format(date1));
//
                int monthChanged = Integer.parseInt(dateFormat.format(date1));
                int monthCurrent = Integer.parseInt(dateFormat.format(date));
//                if (monthChanged == monthCurrent)
//                    calendarView.setSelectedDayGreen(distinctAvailableDatesList);
//                else
                calendarView.setSelectedDayRed(monthCurrent);

            }
        });

    }


    ///////////////------- Photo Upload -----------///////////
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(TherapistUploadProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);
        BitmapFactory.Options options = new BitmapFactory.Options();

        Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);

        Bitmap scaledBitmap = ScalingUtilities.createScaledBitmap(bm, 150, 150, ScalingUtilities.ScalingLogic.CROP);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        profileImage = scaledBitmap;
        profileIV.setImageBitmap(scaledBitmap);
        IMAGESET = 1;

    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        int maxHeight = 150;
        int maxWidth = 150;
        float scale = Math.min(((float) maxHeight / thumbnail.getWidth()), ((float) maxWidth / thumbnail.getHeight()));

        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);
        Bitmap scaledBitmap = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 70, stream);
        byte[] byteArrayImage = stream.toByteArray();
        encodedImage = Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);

        profileImage = scaledBitmap;
        profileIV.setImageBitmap(scaledBitmap);
        IMAGESET = 1;
//        imageUpload();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {

                onSelectFromGalleryResult(data);

            } else if (requestCode == REQUEST_CAMERA) {

                onCaptureImageResult(data);

            }

        }
    }
    ////////////------- End Photo Upload ---------////////////


    //////////------ set Appointment time dialog ---------//////////

    public void setAppointmentTimeDialog(Date date) {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.set_appointment_time_dialog
                , null));
        settingsDialog.setCancelable(false);
        final ListView timeLV = (ListView) settingsDialog.findViewById(R.id.setTimeLV);
        Button bookBTN = (Button) settingsDialog.findViewById(R.id.setTimeBTN);
        Button cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);
        ConstantVal.selectedPosition = -1;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String selectedDate = dateFormat.format(date);

        timeLV.setAdapter(new SetAppointmentTimingsAdapter(SplashActivity.setAppointmentTimeList, TherapistUploadProfileActivity.this));
        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });

        bookBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String slec = "";
                if (ConstantVal.avialAllreadyTimeList.size() > 0) {
                    for (String s : ConstantVal.avialAllreadyTimeList) {
                        slec += s + "$$";
                    }
                    slec = slec.replaceAll(" ", ":");
                    slec = slec.substring(0, slec.length() - 2);
                    Log.e("Checked Count", slec);
                    if (CheckConnectivity.checkInternetConnection(TherapistUploadProfileActivity.this)){
                        callSetScheduleWebservice(selectedDate, slec, settingsDialog);
                    }else {
                        CheckConnectivity.noNetMeg(TherapistUploadProfileActivity.this);
                    }

                } else {
                    Toast.makeText(TherapistUploadProfileActivity.this, "Please select time.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        settingsDialog.show();


    }

    //------------ Agree Dialog -----------//
    public void setAgreeDialog() {

        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.agree_dialog
                , null));
        settingsDialog.setCancelable(false);

        final CheckBox checkBox = (CheckBox) settingsDialog.findViewById(R.id.checkBox2);
        Button agreeBTN = (Button) settingsDialog.findViewById(R.id.agreeBTN);

        agreeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    doneBTN.setText("Done");
                    settingsDialog.dismiss();

                } else {
                    Toast.makeText(TherapistUploadProfileActivity.this,
                            "to proceed further please agree terms & conditions.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        settingsDialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.uploadBTN:
                selectImage();
                break;
            case R.id.doneBTN:
                String speciality = specialityET.getText().toString().trim();
                String rates = ratesET.getText().toString().trim();
                if (doneBTN.getText().toString().trim().equalsIgnoreCase("Save")) {

                    if (speciality.length() > 0 && rates.length() > 0) {
                        if (AVAIL == 1 && IMAGESET == 1) {
                            setAgreeDialog();
                        } else {
                            if (AVAIL == 0) {
                                Toast.makeText(TherapistUploadProfileActivity.this, "Please select your availability.", Toast.LENGTH_SHORT).show();
                            } else if (IMAGESET == 0) {
                                Toast.makeText(TherapistUploadProfileActivity.this, "Please select profile picture.", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else {
                        if (speciality.length() == 0)
                            specialityET.setError("Please enter speciality & experience.");
                        else if (rates.length() == 0)
                            ratesET.setError("Please enter rates.");
                    }
                } else if (doneBTN.getText().toString().trim().equalsIgnoreCase("Done")) {
                    if (CheckConnectivity.checkInternetConnection(TherapistUploadProfileActivity.this)){
                        imageUpload(speciality, rates);
                    }else {
                        CheckConnectivity.noNetMeg(TherapistUploadProfileActivity.this);
                    }

                }

                break;

        }
    }

    public void imageUpload(final String speciality, final String rates) {
//        getStringImage(bitmap);
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Uploading ....");
        ConstantVal.progressDialog.show();


//        String uploadurl = "http://designer321.com/johnilbwd/massageportllc/api/update_therapist.php";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.updateTherapistStatus,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");

                            if (message_code == 1) {

//                                profileIV.setImageBitmap(photo);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putInt("upload", 1);
                                editor.commit();
                                ConstantVal.progressDialog.dismiss();


                                Intent intent = new Intent(TherapistUploadProfileActivity.this, TherapistTabsActivity.class);
                                startActivity(intent);
                                finish();

                                Toast.makeText(TherapistUploadProfileActivity.this, "Profile Uploaded successfully", Toast.LENGTH_SHORT).show();

                            } else {
                                String message = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();

                Toast.makeText(TherapistUploadProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = encodedImage;
                Map<String, String> params = new Hashtable<String, String>();

                params.put("user_id", String.valueOf(user_id));
                params.put("speciality", speciality);
                params.put("rates", rates);
                params.put("profile_photo", image);

                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }

    public void callSetScheduleWebservice(final String selectedDate, final String slecTime, final Dialog settingsDialog) {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/massageportllc/api/set_schedule_of_therapist.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {

                                AVAIL = 1;
                                distinctAvailableDatesList.add(selectedDate);
                                calendarView.setSelectedDayGreen(distinctAvailableDatesList);
                                ConstantVal.progressDialog.dismiss();
                                settingsDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistUploadProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("times", slecTime);
                params.put("date", selectedDate);
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void callAvailStatusWebservice() {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/massageportllc/api/update_therapist_status.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {

                                if (ConstantVal.AVAILABLE_STATUS.equalsIgnoreCase("Yes")) {
//                                    status = "on";

                                    statusBTN.setImageResource(R.drawable.availible_off_button);

                                } else if (ConstantVal.AVAILABLE_STATUS.equalsIgnoreCase("No")){
//                                    status = "off";
                                    statusBTN.setImageResource(R.drawable.availible_on_button);

                                }
                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistUploadProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void checkStatus() {
//        getStringImage(bitmap);
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Uploading ....");
        ConstantVal.progressDialog.show();



        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getCurrentStatus,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");


                            if (message_code == 1) {
                                String message = jobj.getString("message");
//                                profileIV.setImageBitmap(photo);
                                ConstantVal.AVAILABLE_STATUS = jobj.getString("available_status");
                                if (ConstantVal.AVAILABLE_STATUS.equalsIgnoreCase("Yes")){
                                    statusBTN.setImageResource(R.drawable.availible_on_button);
                                }else {
                                    statusBTN.setImageResource(R.drawable.availible_off_button);
                                }
                                ConstantVal.progressDialog.dismiss();

                            } else {
                                String message = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();

                Toast.makeText(TherapistUploadProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));


                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }

//    public void addTimeSlots() {
//        setAppointmentTimeList = new ArrayList<String>();
//        setAppointmentTimeList.add("12:00 AM");
//        setAppointmentTimeList.add("12:30 AM");
//        setAppointmentTimeList.add("01:00 AM");
//        setAppointmentTimeList.add("01:30 AM");
//        setAppointmentTimeList.add("02:00 AM");
//        setAppointmentTimeList.add("02:30 AM");
//        setAppointmentTimeList.add("03:00 AM");
//        setAppointmentTimeList.add("03:30 AM");
//        setAppointmentTimeList.add("04:00 AM");
//        setAppointmentTimeList.add("04:30 AM");
//        setAppointmentTimeList.add("05:00 AM");
//        setAppointmentTimeList.add("05:30 AM");
//        setAppointmentTimeList.add("06:00 AM");
//        setAppointmentTimeList.add("06:30 AM");
//        setAppointmentTimeList.add("07:00 AM");
//        setAppointmentTimeList.add("07:30 AM");
//        setAppointmentTimeList.add("08:00 AM");
//        setAppointmentTimeList.add("08:30 AM");
//        setAppointmentTimeList.add("09:00 AM");
//        setAppointmentTimeList.add("09:30 AM");
//        setAppointmentTimeList.add("10:00 AM");
//        setAppointmentTimeList.add("10:30 AM");
//        setAppointmentTimeList.add("11:00 AM");
//        setAppointmentTimeList.add("11:30 AM");
//        setAppointmentTimeList.add("12:00 PM");
//        setAppointmentTimeList.add("12:30 PM");
//        setAppointmentTimeList.add("01:00 PM");
//        setAppointmentTimeList.add("01:30 PM");
//        setAppointmentTimeList.add("02:00 PM");
//        setAppointmentTimeList.add("02:30 PM");
//        setAppointmentTimeList.add("03:00 PM");
//        setAppointmentTimeList.add("03:30 PM");
//        setAppointmentTimeList.add("04:00 PM");
//        setAppointmentTimeList.add("04:30 PM");
//        setAppointmentTimeList.add("05:00 PM");
//        setAppointmentTimeList.add("05:30 PM");
//        setAppointmentTimeList.add("06:00 PM");
//        setAppointmentTimeList.add("06:30 PM");
//        setAppointmentTimeList.add("07:00 PM");
//        setAppointmentTimeList.add("07:30 PM");
//        setAppointmentTimeList.add("08:00 PM");
//        setAppointmentTimeList.add("08:30 PM");
//        setAppointmentTimeList.add("09:00 PM");
//        setAppointmentTimeList.add("09:30 PM");
//        setAppointmentTimeList.add("10:00 PM");
//        setAppointmentTimeList.add("10:30 PM");
//        setAppointmentTimeList.add("11:00 PM");
//        setAppointmentTimeList.add("11:30 PM");
//
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 2)
            getMenuInflater().inflate(R.menu.therapist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
