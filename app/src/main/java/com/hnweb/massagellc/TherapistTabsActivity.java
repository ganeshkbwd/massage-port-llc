package com.hnweb.massagellc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.fragments.MessagesFragment;
import com.hnweb.massagellc.fragments.MyAppointmentFragment;
import com.hnweb.massagellc.fragments.MyProfileFragment;
import com.hnweb.massagellc.fragments.PaymentFragment;
import com.hnweb.massagellc.fragments.ViewMapFragment;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class TherapistTabsActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ImageView statusBTN, logoIV;
    int login, user_id, upload;
    String status = "off";
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    Button instructionBTN;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapist_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        bundle = getIntent().getExtras();
        logoIV = (ImageView) toolbar.findViewById(R.id.logoIV);
        statusBTN = (ImageView) toolbar.findViewById(R.id.statusIV);
        instructionBTN = (Button) toolbar.findViewById(R.id.instructionBTN);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        instructionBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instructionsDialog();
            }
        });
        checkStatus();
        statusBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckConnectivity.checkInternetConnection(TherapistTabsActivity.this)) {
                    callAvailStatusWebservice(TherapistTabsActivity.this);
                } else {
                    CheckConnectivity.noNetMeg(TherapistTabsActivity.this);
                }

            }
        });

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);
        if (bundle != null) {
            String come = bundle.getString("comeFrom");
            if (come.equalsIgnoreCase("MyProfile")) {
                viewPager.setCurrentItem(1);
            }
        } else {
            viewPager.setCurrentItem(3);
        }


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 3) {
                    statusBTN.setVisibility(View.GONE);
                    instructionBTN.setVisibility(View.VISIBLE);
                } else {

                    statusBTN.setVisibility(View.VISIBLE);
                    instructionBTN.setVisibility(View.GONE);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        if (tabLayout.getSelectedTabPosition() == 3) {
            statusBTN.setVisibility(View.GONE);
            instructionBTN.setVisibility(View.VISIBLE);
        }



        setupTabIcons();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    private void setupTabIcons() {
        XmlResourceParser parser = getResources().getXml(R.xml.tab_text_color);
        XmlResourceParser parser1 = getResources().getXml(R.xml.tab_text_color1);
        XmlResourceParser parser2 = getResources().getXml(R.xml.tab_text_color2);
        XmlResourceParser parser3 = getResources().getXml(R.xml.tab_text_color3);
        XmlResourceParser parser4 = getResources().getXml(R.xml.tab_text_color3);
        ColorStateList colors = null, colors2 = null, colors3 = null, colors4 = null, colors5 = null;
        try {
            colors = ColorStateList.createFromXml(getResources(), parser);
            colors2 = ColorStateList.createFromXml(getResources(), parser1);
            colors3 = ColorStateList.createFromXml(getResources(), parser2);
            colors4 = ColorStateList.createFromXml(getResources(), parser3);
            colors5 = ColorStateList.createFromXml(getResources(), parser3);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("MY PROFILE");
        tabOne.setTextColor(colors);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tab_profile, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabFive = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFive.setText("MESSAGE");
        tabFive.setTextColor(colors4);
        tabFive.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tab_message, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabFive);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("MY APPOINTMENTS");
        tabTwo.setTextColor(colors2);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tab_appointment, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("VIEW MAP");
        tabThree.setTextColor(colors3);
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tab_map, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("PAYMENT");
        tabFour.setTextColor(colors4);
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tab_payment, 0, 0);
        tabLayout.getTabAt(4).setCustomView(tabFour);


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MyProfileFragment(), "MY PROFILE");
        adapter.addFragment(new MessagesFragment(), "MESSAGES");
        adapter.addFragment(new MyAppointmentFragment(), "MY APPOINTMENT");
        adapter.addFragment(new ViewMapFragment(), "VIEW MAP");
        adapter.addFragment(new PaymentFragment(), "PAYMENT");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 2)
            getMenuInflater().inflate(R.menu.therapist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    public void instructionsDialog() {
        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.instruction_dialog
                , null));
        settingsDialog.setCancelable(false);

        Button submitBTN = (Button) settingsDialog.findViewById(R.id.fpsubmitBTN);
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });

        settingsDialog.show();
    }

    public void callAvailStatusWebservice(final Activity context) {

        ConstantVal.progressDialog = new ProgressDialog(context);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/massageportllc/api/update_therapist_status.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {

                                if (status.equalsIgnoreCase("off")) {
                                    status = "on";
                                    statusBTN.setImageResource(R.drawable.availible_off_button);

                                } else {
                                    status = "off";
                                    statusBTN.setImageResource(R.drawable.availible_on_button);

                                }
                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(context, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void checkStatus() {
//        getStringImage(bitmap);
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getCurrentStatus,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");


                            if (message_code == 1) {
//                                String message = jobj.getString("message");
//                                profileIV.setImageBitmap(photo);
                                ConstantVal.AVAILABLE_STATUS = jobj.getString("available_status");
                                if (ConstantVal.AVAILABLE_STATUS.equalsIgnoreCase("Yes")) {
                                    statusBTN.setImageResource(R.drawable.availible_on_button);
                                } else {
                                    statusBTN.setImageResource(R.drawable.availible_off_button);
                                }
                                ConstantVal.progressDialog.dismiss();

                            } else {
//                                String message = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                            ConstantVal.progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();

                Toast.makeText(TherapistTabsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }


}
