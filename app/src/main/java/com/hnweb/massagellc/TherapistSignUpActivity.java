package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TherapistSignUpActivity extends AppCompatActivity implements View.OnClickListener {
    EditText nameET, emailET, confirm_emailET, usernameET, passET, confirm_passET, addressET;
    String name, email, confirm_email, username, pass, confirm_pass;
    Methodes methodes = new Methodes();
    double latitude, longitude;
    Geocoder geocoder;
    List<Address> addresses;
    String address = "", city = "", state, country = "", postalCode, subLocality;
//    private CallbackManager mCallbackManager;
//    private Profile profile;
//    private AccessTokenTracker tokenTracker;
//    private ProfileTracker profileTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_therapist_sign_up);

        init();
        changeStatusBarColor();
    }

//    public void fbadd(){
//        //        callbackManager = CallbackManager.Factory.create();
//        /////////////////////////////////////////
//        keyHash();
//        mCallbackManager = CallbackManager.Factory.create();
//        //   LoginManager.getInstance().logOut();
//        tokenTracker=new AccessTokenTracker() {
//            @Override
//            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {
//
//            }
//        };
//        profileTracker=new ProfileTracker() {
//            @Override
//            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
//                //  textView.setText(displayMessage(profile1));
//                // System.out.println(displayMessage(profile1));
//            }
//        };
//
//        tokenTracker.startTracking();
//        profileTracker.startTracking();
//
//
//        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setCompoundDrawables(null, null, null, null);
//        loginButton.setReadPermissions("user_friends");
//        loginButton.setReadPermissions("email");
//        loginButton.setText("");
//        // loginButton.setActi(this);
//        loginButton.registerCallback(mCallbackManager, mFacebookCallback);
//        //////////////////////////////////////////////
//    }
//
//    public void keyHash() {
//        try {
//            PackageInfo info;
//            info = getPackageManager().getPackageInfo("com.hnweb.massagellc", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md;
//                md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String something = new String(Base64.encode(md.digest(), 0));
//                //String something = new String(Base64.encodeBytes(md.digest()));
//                Log.e("hash key", something);
//            }
//        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
//        } catch (Exception e) {
//            Log.e("exception", e.toString());
//        }
//    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void init() {
//        fbadd();
        nameET = (EditText) findViewById(R.id.nameET);
        emailET = (EditText) findViewById(R.id.email_idET);
        confirm_emailET = (EditText) findViewById(R.id.confirm_emailET);
        usernameET = (EditText) findViewById(R.id.userNameET);
        passET = (EditText) findViewById(R.id.passET);
        confirm_passET = (EditText) findViewById(R.id.confirmPassET);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signmeUpBTN:
                validation();
                break;
        }
    }

    public void validation() {

        name = nameET.getText().toString().trim();
        email = emailET.getText().toString().trim();
        confirm_email = confirm_emailET.getText().toString().trim();
        username = usernameET.getText().toString().trim();
        pass = passET.getText().toString().trim();
        confirm_pass = confirm_passET.getText().toString().trim();

        if (name.length() > 0) {
            if (Patterns.EMAIL_ADDRESS.matcher(email).matches() && email.length() > 0) {
                if (Patterns.EMAIL_ADDRESS.matcher(confirm_email).matches() && email.length() > 0 && confirm_email.equals(email)) {
                    if (username.length() > 0) {
                        if (pass.length() > 0) {
                            if (confirm_pass.length() > 0 && confirm_pass.equals(pass)) {
                                if (CheckConnectivity.checkInternetConnection(TherapistSignUpActivity.this)) {
                                    callSignUpWebservice(name, email, username, pass);
                                } else {
                                    CheckConnectivity.noNetMeg(TherapistSignUpActivity.this);
                                }

                            } else {
                                confirm_passET.setError("Please enter confirm password.");
                            }
                        } else {
                            passET.setError("Please enter password.");
                        }
                    } else {
                        usernameET.setError("Please enter username");
                    }
                } else {
                    confirm_emailET.setError("Please enter valid confirm email id.");
                }
            } else {
                emailET.setError("Please enter valid email id.");
            }
        } else {
            nameET.setError("Please enter name.");
        }


    }


    public void callSignUpWebservice(final String name, final String email, final String username, final String pass) {

        latitude = methodes.myLocation(TherapistSignUpActivity.this).getLatitude();
        longitude = methodes.myLocation(TherapistSignUpActivity.this).getLongitude();
        final String address = getAddressFromLatLong(latitude, longitude);

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.therapistRegistration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                Toast.makeText(TherapistSignUpActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();


                                Intent intent = new Intent(TherapistSignUpActivity.this, TherapistSignInActivity.class);
                                startActivity(intent);
                                finish();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(TherapistSignUpActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistSignUpActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("full_name", name);
                params.put("email_address", email);
                params.put("username", username);
                params.put("password", pass);
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));
                params.put("full_address", address);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public String getAddressFromLatLong(double lattitude, double longitude) {
        String fulladdress = "";
        geocoder = new Geocoder(TherapistSignUpActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lattitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            subLocality = addresses.get(0).getSubLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        fulladdress = address + "," + subLocality + "," + city + "," + state + "," + country;
        return fulladdress;
    }

//    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
//        @Override
//        public void onSuccess(LoginResult loginResult) {
//            AccessToken accessToken = loginResult.getAccessToken();
//            profile = Profile.getCurrentProfile();
//
//
//            GraphRequest request = GraphRequest.newMeRequest(
//                    loginResult.getAccessToken(),
//                    new GraphRequest.GraphJSONObjectCallback() {
//                        @Override
//                        public void onCompleted(
//                                JSONObject object,
//                                GraphResponse response) {
//                            Log.v("LoginActivity Response ", response.toString());
//                            System.out.println("arshres"+response.toString());
//
//                            try {
//                                String Name = object.getString("name");
//
//                                String FEmail = object.getString("email");
//                                Log.v("Email = ", " " + FEmail);
//                                // Toast.makeText(getApplicationContext(), "Name " + Name+FEmail, Toast.LENGTH_LONG).show();
//
////                                if(module.toString().equals("Customer"))
////                                {
////                                    social_media_login_customer(Name,FEmail);
////                                }
////
////                                else
////                                {
////                                    social_media_login_driver(Name,FEmail);
////                                }
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//            Bundle parameters = new Bundle();
//            parameters.putString("fields", "id,name,email,gender, birthday");
//            request.setParameters(parameters);
//            request.executeAsync();
//
//
//
//
//            //textView.setText(displayMessage(profile));
//        }
//
//        @Override
//        public void onCancel() {
//
//        }
//
//        @Override
//        public void onError(FacebookException e) {
//
//        }
//    };
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mCallbackManager.onActivityResult(requestCode, resultCode, data);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        Profile profile = Profile.getCurrentProfile();
//        //   textView.setText(displayMessage(profile));
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        profileTracker.stopTracking();
//        tokenTracker.stopTracking();
//    }
//
//    private String displayMessage(Profile profile) {
//        StringBuilder stringBuilder = new StringBuilder();
//        if (profile != null) {
//            stringBuilder.append("Logged In "+profile.getFirstName()+profile.getProfilePictureUri(100,100));
//            Toast.makeText(getApplicationContext(), "Start Playing with the data "+profile.getFirstName(), Toast.LENGTH_SHORT).show();
//        }else{
//            stringBuilder.append("You are not logged in");
//        }
//        return stringBuilder.toString();
//    }

}
