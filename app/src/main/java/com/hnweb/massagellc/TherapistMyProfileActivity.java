package com.hnweb.massagellc;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.ShowAvialTimeAdapter;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.AllTherapists;
import com.squareup.picasso.Picasso;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class TherapistMyProfileActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ArrayList<String> distinctAvailableDatesList = new ArrayList<String>();
    TextView fullname, speciality, rates, status;
    ImageView profile;
    Button viewReview;
    RatingBar rating;
    CustomCalendarView calendarView;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id, position;
    ArrayList<AllTherapists> atList = new ArrayList<AllTherapists>();
    ArrayList<String> selectedTime = new ArrayList<String>();
    ImageView statusBTN;
    String status1 = "off";
    Button msgBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        statusBTN = (ImageView) toolbar.findViewById(R.id.statusIV);

        statusBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckConnectivity.checkInternetConnection(TherapistMyProfileActivity.this)) {
                    callAvailStatusWebservice(TherapistMyProfileActivity.this);
                } else {
                    CheckConnectivity.noNetMeg(TherapistMyProfileActivity.this);
                }

            }
        });

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        init();
        getFromBundle();


        setCalenderView();
    }

    public void init() {
        fullname = (TextView) findViewById(R.id.fullNameTV);
        rating = (RatingBar) findViewById(R.id.ratingTV);
        speciality = (TextView) findViewById(R.id.specialityTV);
        rates = (TextView) findViewById(R.id.ratesTV);
        status = (TextView) findViewById(R.id.statusTV);
        profile = (ImageView) findViewById(R.id.profileIV);
        viewReview = (Button) findViewById(R.id.viewReviewBTN);
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);
        msgBTN = (Button) findViewById(R.id.msgBTN);

        speciality.setMovementMethod(new ScrollingMovementMethod());
        rates.setMovementMethod(new ScrollingMovementMethod());
//        rating.setRating(Float.parseFloat(atList.get(position).getAvg_rating()));
        LayerDrawable stars = (LayerDrawable) rating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
//if ()
        viewReview.setOnClickListener(this);

    }

    public void setData() {
        fullname.setText(atList.get(position).getFull_name());


        speciality.setText(atList.get(position).getSpeciality());
        rates.setText(atList.get(position).getRates());
        rating.setRating(Float.parseFloat(atList.get(position).getAvg_rating()));
//        if (filelist.get(position).getAvailable_status().equalsIgnoreCase("Yes")) {
//            status.setText("This Massage Therapist is Avialable Now!!!");
//        } else {
//            status.setText("This Massage Therapist is Not Avialable Now!!!");
//        }

        if (!atList.get(position).getProfile_photo().equals("")) {
            Picasso.with(this).load(atList.get(position).getProfile_photo()).into(profile);
        }

        if (user_id == atList.get(position).getUser_id()) {
            viewReview.setVisibility(View.VISIBLE);
            msgBTN.setVisibility(View.INVISIBLE);

        } else {
            viewReview.setVisibility(View.INVISIBLE);
            msgBTN.setVisibility(View.VISIBLE);
        }
//        Calendar calendar = Calendar.getInstance();
//        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void setCalenderView() {
        //Initialize calendar with date
        final Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);

        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                Toast.makeText(TherapistProfileActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
                ConstantVal.date_selected = df.format(date);
                if (distinctAvailableDatesList.contains(ConstantVal.date_selected)) {
//                    callweservicesToBook(df.format(date));
                    if (CheckConnectivity.checkInternetConnection(TherapistMyProfileActivity.this)) {
                        callweservicesToBook(df.format(date), date);
                    } else {
                        CheckConnectivity.noNetMeg(TherapistMyProfileActivity.this);
                    }


                } else {
                    Toast.makeText(TherapistMyProfileActivity.this, "To update schedule go to My Profile and update status.", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onMonthChanged(Date date) {

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                int monthChanged = Integer.parseInt(dateFormat.format(date1));
//                int monthCurrent = Integer.parseInt(dateFormat.format(date));
//                if (monthChanged == monthCurrent)
                Log.e("Date", df.format(date));
                calendarView.setNewDateColor(distinctAvailableDatesList, df.format(date));
////                calendarView.refreshCalendar(currentCalendar);
//                DateFormat dateFormat = new SimpleDateFormat("MM");
//                Date date1 = new Date();
//                Log.e("Month", dateFormat.format(date1));
////
//                int monthChanged = Integer.parseInt(dateFormat.format(date1));
//                int monthCurrent = Integer.parseInt(dateFormat.format(date));
//                if (monthChanged == monthCurrent)
//                    calendarView.setSelectedDayGreen(distinctAvailableDatesList);
//                else
//                    calendarView.setSelectedDayRed(monthCurrent);


//                Toast.makeText(TherapistProfileActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getFromBundle() {
        Bundle b = getIntent().getExtras();
        if (b != null) {
            atList = (ArrayList<AllTherapists>) b.getSerializable("ALLTherapist");
            position = b.getInt("Position");
            setData();
            if (CheckConnectivity.checkInternetConnection(TherapistMyProfileActivity.this)) {
                checkStatus();

            } else {
                CheckConnectivity.noNetMeg(TherapistMyProfileActivity.this);
            }

        }

    }

    private void getDistinctAvailableDates() {
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getDistinctAvailableDates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                distinctAvailableDatesList.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String distinct_date = jsonArray.getJSONObject(i).getString("date");
                                    String[] month = distinct_date.split("-");

                                    Calendar c = Calendar.getInstance();
                                    int year = c.get(Calendar.YEAR);
                                    int cur_month = c.get(Calendar.MONTH);
                                    if (cur_month == Integer.parseInt(month[1]))
                                        distinctAvailableDatesList.add(distinct_date);

                                }
                                calendarView.setSelectedDayGreen(distinctAvailableDatesList);
//                                String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//                                calendarView.setNewDateColor(distinctAvailableDatesList,date);
                                ConstantVal.progressDialog.dismiss();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistMyProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);
    }

    public void callweservicesToBook(final String date, final Date date1) {
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getAvailableTimings,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                ConstantVal.avialAllreadyTimeList.clear();
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {

//                                    jsonArray.getJSONObject(i).getInt("time_id");
//                                    jsonArray.getJSONObject(i).getInt("therapist_id");
                                    String hour = String.valueOf(jsonArray.getJSONObject(i).getInt("hour"));
                                    String minute = String.valueOf(jsonArray.getJSONObject(i).getInt("minute"));
                                    String period = jsonArray.getJSONObject(i).getString("period");
//                                    jsonArray.getJSONObject(i).getString("date");
//                                    jsonArray.getJSONObject(i).getString("created_date");
//                                    jsonArray.getJSONObject(i).getString("last_updated_date");
//                                    jsonArray.getJSONObject(i).getString("status");

                                    if (Integer.parseInt(hour) < 10) {
                                        hour = "0" + hour;
                                    }
                                    if (Integer.parseInt(minute) < 10) {
                                        minute = "0" + minute;
                                    }
                                    String time = hour + ":" + minute + " " + period;
                                    ConstantVal.avialAllreadyTimeList.add(time);

                                }
//                                setAvailTimeDialog(availableTimings, date);
//                                Log.e("LIST", therapistDataList.toString());

                                ConstantVal.progressDialog.dismiss();
                                setAppointmentTimeDialog(date1);
//                                settingsDialog.dismiss();
//                                Toast.makeText(context, ConstantVal.RESPONSE, Toast.LENGTH_SHORT).show();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

//                                Toast.makeText(SelectionActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistMyProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                params.put("date_of_schedule", date);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);

    }

    public void setAppointmentTimeDialog(Date date) {

        final Dialog settingsDialog = new Dialog(TherapistMyProfileActivity.this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(TherapistMyProfileActivity.this.getLayoutInflater().inflate(R.layout.set_appointment_time_dialog
                , null));
        settingsDialog.setCancelable(false);
        final ListView timeLV = (ListView) settingsDialog.findViewById(R.id.setTimeLV);
        Button bookBTN = (Button) settingsDialog.findViewById(R.id.setTimeBTN);
        bookBTN.setVisibility(View.GONE);
        Button cancelBTN = (Button) settingsDialog.findViewById(R.id.cancelBTN);
        cancelBTN.setText("OK");

//        ConstantVal.selectedPosition = -1;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String selectedDate = dateFormat.format(date);

        timeLV.setAdapter(new ShowAvialTimeAdapter(ConstantVal.avialAllreadyTimeList, TherapistMyProfileActivity.this));
        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });


        settingsDialog.show();


    }

    public void callAvailStatusWebservice(final Activity context) {

        ConstantVal.progressDialog = new ProgressDialog(context);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://designer321.com/johnilbwd/massageportllc/api/update_therapist_status.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {

                                if (status1.equalsIgnoreCase("off")) {
                                    status1 = "on";
                                    statusBTN.setImageResource(R.drawable.availible_off_button);

                                } else {
                                    status1 = "off";
                                    statusBTN.setImageResource(R.drawable.availible_on_button);

                                }
                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(context, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));

                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewReviewBTN:
                Intent intent = new Intent(TherapistMyProfileActivity.this, TherapistMyReviewActivity.class);
                startActivity(intent);

                break;
            case R.id.msgBTN:
                Intent intent1 = new Intent(TherapistMyProfileActivity.this, MessageToCustomerActivity.class);
                intent1.putExtra("ID", String.valueOf(atList.get(position).getUser_id()));
                intent1.putExtra("NAME", atList.get(position).getFull_name());
                intent1.putExtra("comeFrom", "MyProfile");
                startActivity(intent1);
                finish();

                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 2)
            getMenuInflater().inflate(R.menu.therapist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    public void checkStatus() {
//        getStringImage(bitmap);
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getCurrentStatus,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);

                            int message_code = jobj.getInt("message_code");
//                            String message = jobj.getString("message");


                            if (message_code == 1) {
                                String message = jobj.getString("message");
//                                profileIV.setImageBitmap(photo);
                                ConstantVal.AVAILABLE_STATUS = jobj.getString("available_status");
                                if (ConstantVal.AVAILABLE_STATUS.equalsIgnoreCase("Yes")) {
                                    statusBTN.setImageResource(R.drawable.availible_on_button);
                                } else {
                                    statusBTN.setImageResource(R.drawable.availible_off_button);
                                }
                                ConstantVal.progressDialog.dismiss();
                                getDistinctAvailableDates();

                            } else {
//                                String message = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();
//                                Toast.makeText(MonthProgressActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
//                            ConstantVal.progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();

                Toast.makeText(TherapistMyProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };


        queue.add(stringRequest);

    }
}
