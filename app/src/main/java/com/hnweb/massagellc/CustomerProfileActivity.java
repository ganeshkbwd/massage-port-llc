package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.GeocodingLocation;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.CustomerProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class CustomerProfileActivity extends AppCompatActivity implements View.OnClickListener {
    ListView customerProfileLV;
    ArrayList<String> cpList = new ArrayList<String>();
    int get_user_id;
    String full_name, address, email_address;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    ArrayList<String> titlesList;
    Button editProfileBTN;
    EditText nameET, addressET, email_idET;
    int user_id, login;
    Toolbar toolbar;
    private static boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);
        init();
        if (CheckConnectivity.checkInternetConnection(this)) {
            getcustomerProfileWebService(user_id);
        } else {
            CheckConnectivity.noNetMeg(this);
        }



    }

    public void init() {
        //        customerProfileLV = (ListView) findViewById(R.id.custProfileLV);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        editProfileBTN = (Button) findViewById(R.id.editProfileBTN);
        nameET = (EditText) findViewById(R.id.nameET);
        addressET = (EditText) findViewById(R.id.addressET);
        email_idET = (EditText) findViewById(R.id.email_idET);

        nameET.setEnabled(false);
        addressET.setEnabled(false);
        email_idET.setEnabled(false);
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }


            String[] latLong = locationAddress.split(",");
            String latitude = latLong[0];
            String longitude = latLong[1];
//            Toast.makeText(CustomerProfileActivity.this, latitude + "\n" + longitude, Toast.LENGTH_SHORT).show();
            if (CheckConnectivity.checkInternetConnection(CustomerProfileActivity.this)) {
                updateCustomerProfileWebService(user_id, latitude, longitude);
            } else {
                CheckConnectivity.noNetMeg(CustomerProfileActivity.this);
            }

//            latLongTV.setText(locationAddress);
        }
    }

    public void getcustomerProfileWebService(final int user_id) {


        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.customerProfile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {

                                get_user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String full_address = jobj.getString("full_address");
                                String email_address = jobj.getString("email_address");
                                nameET.setText(full_name);
                                addressET.setText(full_address);
                                email_idET.setText(email_address);

                                ConstantVal.progressDialog.dismiss();
                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(CustomerProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("customer_id", String.valueOf(user_id));

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void updateCustomerProfileWebService(final int user_id, final String latitude, final String longitude) {


        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.updateCustomerProfile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {

                                nameET.setText(full_name);
                                addressET.setText(address);
                                email_idET.setText(email_address);
                                editProfileBTN.setText("Edit Profile");
                                nameET.setEnabled(false);
                                addressET.setEnabled(false);
                                email_idET.setEnabled(false);
                                nameET.getBackground().setColorFilter(getResources().getColor(android.R.color.transparent), PorterDuff.Mode.SRC_ATOP);
                                addressET.getBackground().setColorFilter(getResources().getColor(android.R.color.transparent), PorterDuff.Mode.SRC_ATOP);
                                email_idET.getBackground().setColorFilter(getResources().getColor(android.R.color.transparent), PorterDuff.Mode.SRC_ATOP);
                                Toast.makeText(CustomerProfileActivity.this, "Your Profile has been updated successfully.", Toast.LENGTH_SHORT).show();

                                ConstantVal.progressDialog.dismiss();
                            } else {
                                Toast.makeText(CustomerProfileActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(CustomerProfileActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_id", String.valueOf(user_id));
                params.put("full_name", full_name);
                params.put("email_address", email_address);
                params.put("full_address", address);
                params.put("latitude", latitude);
                params.put("longitude", longitude);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.viewMapBTN:
                Intent intent1 = new Intent(this, MapsActivity.class);
                intent1.putExtra("BACK","BACK");
                startActivity(intent1);
                finish();
                break;
            case R.id.myAppBTN:
                intent = new Intent(this, MyAppointmentActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.myProfileBTN:
                Toast.makeText(this, "You are currently on My Profile.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.msgBTN:
                intent = new Intent(CustomerProfileActivity.this,TherapistsActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.editProfileBTN:
                if (editProfileBTN.getText().toString().trim().equalsIgnoreCase("Save")) {

                    full_name = nameET.getText().toString().trim();
                    address = addressET.getText().toString().trim();
                    email_address = email_idET.getText().toString().trim();

                    GeocodingLocation locationAddress = new GeocodingLocation();
                    locationAddress.getAddressFromLocation(address,
                            getApplicationContext(), new GeocoderHandler());

                } else {
                    editProfileBTN.setText("Save");
                    nameET.setEnabled(true);
                    addressET.setEnabled(true);
                    email_idET.setEnabled(true);
                    nameET.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                    addressET.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                    email_idET.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                    nameET.setSelection(nameET.getText().toString().trim().length());
                    addressET.setSelection(addressET.getText().toString().trim().length());
                    email_idET.setSelection(email_idET.getText().toString().trim().length());


                }


                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 1)
            getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit from Massage Port,LLC App", Toast.LENGTH_SHORT)
                .show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
