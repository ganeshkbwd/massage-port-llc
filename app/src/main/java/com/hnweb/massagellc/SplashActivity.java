package com.hnweb.massagellc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {


    SharedPreferences sharedPreferences;
    int login, user_id, upload;
    String profile_photo;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    public static ArrayList<String> setAppointmentTimeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        handler();
        changeStatusBarColor();

        addTimeSlots();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void handler() {
        new Handler().postDelayed(new Runnable() {


            // Using handler with postDelayed called runnable run method
            @Override
            public void run() {

                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
                login = sharedPreferences.getInt("login", 0);
                user_id = sharedPreferences.getInt("user_id", 0);
                upload = sharedPreferences.getInt("upload", 0);
                profile_photo = sharedPreferences.getString("profile_photo", "");
                if (login == 1) {
                    Intent intent = new Intent(SplashActivity.this, MapsActivity.class);
                    startActivity(intent);
                    finish();
                } else if (login == 2) {
                    if (profile_photo.equalsIgnoreCase("")) {
                        Intent i = new Intent(SplashActivity.this, TherapistUploadProfileActivity.class);
                        startActivity(i);
                        // close this activity
                        finish();
                    } else {
                        Intent i = new Intent(SplashActivity.this, TherapistTabsActivity.class);
                        startActivity(i);
                        // close this activity
                        finish();
                    }

                } else {
                    Intent i = new Intent(SplashActivity.this, SelectionActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }

            }
        }, 5 * 1000); // wait for 5 seconds
    }


    public void addTimeSlots() {
        setAppointmentTimeList = new ArrayList<String>();
        setAppointmentTimeList.add("12:00 AM");
        setAppointmentTimeList.add("12:30 AM");
        setAppointmentTimeList.add("01:00 AM");
        setAppointmentTimeList.add("01:30 AM");
        setAppointmentTimeList.add("02:00 AM");
        setAppointmentTimeList.add("02:30 AM");
        setAppointmentTimeList.add("03:00 AM");
        setAppointmentTimeList.add("03:30 AM");
        setAppointmentTimeList.add("04:00 AM");
        setAppointmentTimeList.add("04:30 AM");
        setAppointmentTimeList.add("05:00 AM");
        setAppointmentTimeList.add("05:30 AM");
        setAppointmentTimeList.add("06:00 AM");
        setAppointmentTimeList.add("06:30 AM");
        setAppointmentTimeList.add("07:00 AM");
        setAppointmentTimeList.add("07:30 AM");
        setAppointmentTimeList.add("08:00 AM");
        setAppointmentTimeList.add("08:30 AM");
        setAppointmentTimeList.add("09:00 AM");
        setAppointmentTimeList.add("09:30 AM");
        setAppointmentTimeList.add("10:00 AM");
        setAppointmentTimeList.add("10:30 AM");
        setAppointmentTimeList.add("11:00 AM");
        setAppointmentTimeList.add("11:30 AM");
        setAppointmentTimeList.add("12:00 PM");
        setAppointmentTimeList.add("12:30 PM");
        setAppointmentTimeList.add("01:00 PM");
        setAppointmentTimeList.add("01:30 PM");
        setAppointmentTimeList.add("02:00 PM");
        setAppointmentTimeList.add("02:30 PM");
        setAppointmentTimeList.add("03:00 PM");
        setAppointmentTimeList.add("03:30 PM");
        setAppointmentTimeList.add("04:00 PM");
        setAppointmentTimeList.add("04:30 PM");
        setAppointmentTimeList.add("05:00 PM");
        setAppointmentTimeList.add("05:30 PM");
        setAppointmentTimeList.add("06:00 PM");
        setAppointmentTimeList.add("06:30 PM");
        setAppointmentTimeList.add("07:00 PM");
        setAppointmentTimeList.add("07:30 PM");
        setAppointmentTimeList.add("08:00 PM");
        setAppointmentTimeList.add("08:30 PM");
        setAppointmentTimeList.add("09:00 PM");
        setAppointmentTimeList.add("09:30 PM");
        setAppointmentTimeList.add("10:00 PM");
        setAppointmentTimeList.add("10:30 PM");
        setAppointmentTimeList.add("11:00 PM");
        setAppointmentTimeList.add("11:30 PM");

    }

}
