package com.hnweb.massagellc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.GeocodingLocation;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.ShowTherapistData;
import com.hnweb.massagellc.pojo.TherapistData;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private GoogleMap mMap;
    ArrayList<TherapistData> therapistDataList;
    Bitmap bm;
    Marker customMarker;
    Toolbar toolbar;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    Methodes methodes = new Methodes();
    double lattitude;
    double longitude;
    ArrayList<ShowTherapistData> showTherapistData = new ArrayList<ShowTherapistData>();
    int login, user_id;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    Button viewMapBTN, myAppBTN, myProfileBTN;
    EditText findAreaET;
    String findArea;
    private static boolean doubleBackToExitPressedOnce = false;
    private final int ID_MENU_EXIT = 1;
    private Menu mymenu;
    boolean mIsRunning;
    SupportMapFragment mapFragment;
    View markerView;
    View marker;
    Marker markerMap;
    MarkerOptions markerOptions;
    ArrayList<Marker> markerArrayList = new ArrayList<Marker>();
    Button msgBTN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        login = sharedPreferences.getInt("login", 0);
        user_id = sharedPreferences.getInt("user_id", 0);


        init();

    }


    public void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();

        viewMapBTN = (Button) findViewById(R.id.viewMapBTN);
        myAppBTN = (Button) findViewById(R.id.myAppBTN);
        myProfileBTN = (Button) findViewById(R.id.myProfileBTN);
        findAreaET = (EditText) findViewById(R.id.findAreaET);
        msgBTN = (Button) findViewById(R.id.msgBTN);


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);
        marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);


        if (login == 1) {
            viewMapBTN.setVisibility(View.VISIBLE);
            myAppBTN.setVisibility(View.VISIBLE);
            myProfileBTN.setVisibility(View.VISIBLE);
            msgBTN.setVisibility(View.VISIBLE);
        } else {
            viewMapBTN.setVisibility(View.GONE);
            myAppBTN.setVisibility(View.GONE);
            myProfileBTN.setVisibility(View.GONE);
            msgBTN.setVisibility(View.GONE);
        }

    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        if (CheckConnectivity.checkInternetConnection(MapsActivity.this)) {
            locationdialog();
        } else {
            CheckConnectivity.noNetMeg(MapsActivity.this);
        }


    }


    public void getAllTherapist(final double lattitude, final double longitude) {
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Urls.getAllTherapistCustomer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (mMap != null) {
                            mMap.clear();
                        }
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {


                                mMap.clear();
//                                for (Marker marker : markerArrayList) {
//                                    marker.remove();
//                                }
//                                markerMap.remove();
//                                markerMap.
                                markerArrayList.clear();
                                if (showTherapistData.size() != 0) {
                                    Log.e("SIZE", showTherapistData.size() + "");
                                    showTherapistData.clear();
                                    Log.e("SIZE", showTherapistData.size() + "");
                                }


                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Log.e("jsonArray.length()", jsonArray.length() + "");
                                    ShowTherapistData std = new ShowTherapistData();
                                    std.setUser_id(jsonArray.getJSONObject(i).getInt("user_id"));
                                    std.setAvailable_status(jsonArray.getJSONObject(i).getString("available_status"));
                                    std.setFull_name(jsonArray.getJSONObject(i).getString("full_name"));
                                    std.setEmail_address(jsonArray.getJSONObject(i).getString("email_address"));
                                    std.setUsername(jsonArray.getJSONObject(i).getString("username"));
                                    std.setLatitude(jsonArray.getJSONObject(i).getString("latitude"));
                                    std.setLongitude(jsonArray.getJSONObject(i).getString("longitude"));
                                    std.setFull_address(jsonArray.getJSONObject(i).getString("full_address"));
                                    std.setExpired_datetime(jsonArray.getJSONObject(i).getString("expired_datetime"));
                                    std.setProfile_photo(jsonArray.getJSONObject(i).getString("profile_photo"));
                                    std.setSpeciality(jsonArray.getJSONObject(i).getString("speciality"));
                                    std.setRates(jsonArray.getJSONObject(i).getString("rates"));
                                    std.setAvg_rating(jsonArray.getJSONObject(i).getString("avg_rating"));
                                    std.setTotal_reviews(jsonArray.getJSONObject(i).getString("total_reviews"));
                                    showTherapistData.add(std);
//
                                }
                                Log.e("SIZE2", showTherapistData.size() + "");
                                if (showTherapistData != null) {
                                    Log.e("SIZE2.2", showTherapistData.size() + "");
                                    for (int i = 0; i < showTherapistData.size(); i++) {
                                        double lati = Double.parseDouble(showTherapistData.get(i).getLatitude());
                                        double longLat = Double.parseDouble(showTherapistData.get(i).getLongitude());
                                        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
//                                        ImageView markerProfile = (ImageView) markerView.findViewById(R.id.profileIV);
//                                        Picasso.with(MapsActivity.this).load(showTherapistData.get(i).getProfile_photo().toString()).into(markerProfile);
//                                        markerView.setDrawingCacheEnabled(true);
//                                        markerView.buildDrawingCache();
////
//                                        bm = markerView.getDrawingCache();
                                        ImageView profileIv = (ImageView) marker.findViewById(R.id.profileIV);
                                        if (!showTherapistData.get(i).getProfile_photo().toString().equalsIgnoreCase(""))
                                            Picasso.with(MapsActivity.this).load(showTherapistData.get(i).getProfile_photo().toString()).into(profileIv);
                                        RatingBar ratingBar = (RatingBar) marker.findViewById(R.id.ratingBar);
                                        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
                                        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                                        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                                        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                                        ratingBar.setRating(Float.parseFloat(showTherapistData.get(i).getAvg_rating()));
                                        TextView reviewTv = (TextView) marker.findViewById(R.id.reviewsTV);
                                        reviewTv.setText("(" + showTherapistData.get(i).getTotal_reviews().toString() + ")");
                                        TextView statusTV = (TextView) marker.findViewById(R.id.statusTV);
                                        if (showTherapistData.get(i).getAvailable_status().equals("Yes")) {
                                            statusTV.setText("Available Now");
                                        } else
                                            statusTV.setText("Not Available");
//                                            statusTV.setText(showTherapistData.get(i).getAvailable_status());
                                        markerOptions = new MarkerOptions().position(
                                                new LatLng(lati, longLat)).title(String.valueOf(i))
                                                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(MapsActivity.this, marker)));
                                        // markerMap = mMap.addMarker(markerOptions);

                                        Marker mk = mMap.addMarker(markerOptions);
//                                        mk.set
                                        markerArrayList.add(markerMap);
//                                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                                            @Override
//                                            public boolean onMarkerClick(Marker marker) {
//
////                                                int markerPosition = Integer.parseInt(marker.getId().substring(1));
//                                                Bundle bundle = new Bundle();
//
//                                                bundle.putSerializable("List", showTherapistData);
////                                                bundle.putString("Name",showTherapistData.get(markerPosition).getFull_name());
////                                                bundle.putString("Status",showTherapistData.get(markerPosition).getAvailable_status());
////                                                bundle.putString("Speciality",showTherapistData.get(markerPosition).getSpeciality());
////                                                bundle.putString("Rates",showTherapistData.get(markerPosition).getRates());
////                                                bundle.putString("Avg_rating",showTherapistData.get(markerPosition).getAvg_rating());
////                                                bundle.putString("Photo",showTherapistData.get(markerPosition).getProfile_photo());
//
//                                                bundle.putString("Position", marker.getId().substring(1));
//                                                Log.e("Position",marker.getId()+"");
//
//                                                Intent intent = new Intent(MapsActivity.this, TherapistProfileActivity.class);
//                                                intent.putExtra("ComeFrom", "Maps");
//
//                                                intent.putExtras(bundle);
////                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                                startActivity(intent);
//
//
////        startActivityForResult(intent, 100);
//                                                finish();
//                                                return false;
//                                            }
//                                        });
//                                        markerArrayList.add(markerMap);

                                    }
                                    ConstantVal.progressDialog.dismiss();
                                }
                                ConstantVal.progressDialog.dismiss();
//
                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message");
                                ConstantVal.progressDialog.dismiss();//
//                                Toast.makeText(MapsActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }

                            ConstantVal.progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(MapsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
//                params.put("user_id", String.valueOf(filelist.get(position).getUser_id()));
//                params.put("latitude", String.valueOf(lattitude));
//                params.put("longitude", String.valueOf(longitude));
                //returning parameters
                return params;
            }
        };
        RetryPolicy policy = new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                ConstantVal.progressDialog.dismiss();
            }
        }, 20000);
        queue.add(stringRequest);
    }


    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Bundle bundle = new Bundle();

        bundle.putSerializable("List", showTherapistData);

        bundle.putString("ComeFrom", "Maps");
        bundle.putString("Position", marker.getTitle());
        Log.e("MARK ID", String.valueOf(marker.getTitle()));
        Intent intent = new Intent(MapsActivity.this, TherapistProfileActivity.class);


        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);


//        startActivityForResult(intent, 100);
        finish();
//        Toast.makeText(this, "Marker Clicked!!!!" + marker.getId().substring(1), Toast.LENGTH_SHORT).show();
        return true;
    }


    public void locationdialog() {
        if (Methodes.isLocationEnabled(MapsActivity.this)) {

            if (ConstantVal.BACK == 1) {
                lattitude = ConstantVal.lastKnownLat;
                longitude = ConstantVal.lastKnownLongi;
            } else {
                try {
                    lattitude = methodes.myLocation(MapsActivity.this).getLatitude();
                    longitude = methodes.myLocation(MapsActivity.this).getLongitude();

//                    ConstantVal.lastKnownLat = 25.774720;
//                    ConstantVal.lastKnownLongi = -80.135679;
                    ConstantVal.lastKnownLat = lattitude;
                    ConstantVal.lastKnownLongi = longitude;
                } catch (Exception e) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }

            }


//        lattitude = 25.774720;
//        longitude = -80.136579;

            Log.e("Lat", lattitude + " " + longitude);
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(ConstantVal.lastKnownLat,
                            ConstantVal.lastKnownLongi));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(7);

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
            mMap.setOnMarkerClickListener(this);
//            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                return;
//            }
//            mMap.setMyLocationEnabled(true);
            if (CheckConnectivity.checkInternetConnection(MapsActivity.this)) {
                getAllTherapist(lattitude, longitude);
            } else {
                CheckConnectivity.noNetMeg(MapsActivity.this);
            }


        } else {
            final Dialog settingsDialog = new Dialog(this);
            settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.location_enabled_layout
                    , null));
            settingsDialog.setCancelable(false);
            Button submitBTN = (Button) settingsDialog.findViewById(R.id.dontallowBTN);
            submitBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                    settingsDialog.dismiss();
                }
            });

            Button allowBTN = (Button) settingsDialog.findViewById(R.id.allowBTN);
            allowBTN.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//                    intent.putExtra("enabled", true);
//                    sendBroadcast(intent);
                    settingsDialog.dismiss();

                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(myIntent, 100);
                }
            });
            settingsDialog.show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);

        }
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.submitBTN:
                findArea = findAreaET.getText().toString().trim();
                if (findArea.length() > 0) {
//                    if (containsOnlyNumbers(findArea)) {
//                        //zipCode
//                        String[] latLongi = getLatLongFromZip(findArea).split(",");
////                        lattitude = ;
////                        longitude = ;
//                        CameraUpdate center =
//                                CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(latLongi[0]),
//                                        Double.parseDouble(latLongi[1])));
////                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
//
//                        mMap.moveCamera(center);
////                        mMap.animateCamera(zoom);
//                        getAllTherapist(Double.parseDouble(latLongi[0]), Double.parseDouble(latLongi[1]));
//
//                    } else {
                    //address

                    GeocodingLocation locationAddress = new GeocodingLocation();
                    locationAddress.getAddressFromLocation(findArea,
                            getApplicationContext(), new GeocoderHandler());


//                    }

                } else {
                    findAreaET.setError("Please give input");

                }

//                Toast.makeText(MapsActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                break;
            case R.id.viewMapBTN:
                Toast.makeText(MapsActivity.this, "You are currently on View Map", Toast.LENGTH_SHORT).show();

                break;
            case R.id.myAppBTN:
                intent = new Intent(this, MyAppointmentActivity.class);
                startActivity(intent);
                finish();
//                Toast.makeText(MapsActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                break;
            case R.id.myProfileBTN:
//                Toast.makeText(MapsActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                intent = new Intent(MapsActivity.this, CustomerProfileActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.msgBTN:
                intent = new Intent(MapsActivity.this, TherapistsActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    public static boolean containsOnlyNumbers(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i)))
                return false;
        }
        return true;
    }

    public String getLatLongFromZip(String findArea) {
        final Geocoder geocoder = new Geocoder(this);
        final String zip = findArea;
        try {
            List<Address> addresses = geocoder.getFromLocationName(zip, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                // Use the address as needed
                String message = String.format("%f,%f",
                        address.getLatitude(), address.getLongitude());

//                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                return message;
            } else {
                // Display appropriate message when Geocoder services are not available
                Toast.makeText(this, "Unable to geocode zipcode", Toast.LENGTH_LONG).show();
                return null;
            }
        } catch (IOException e) {
            // handle exception
            return null;
        }
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }


            String[] latLong = locationAddress.split(",");
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(latLong[0]),
                            Double.parseDouble(latLong[1])));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(07);

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
            getAllTherapist(Double.parseDouble(latLong[0]), Double.parseDouble(latLong[1]));

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 1)
            getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.setting_button));
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (login == 1) {
            if (doubleBackToExitPressedOnce) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click back again to exit from Massage Port,LLC App", Toast.LENGTH_SHORT)
                    .show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        } else {
            Intent intent = new Intent(this, SelectionActivity.class);
            startActivity(intent);
            finish();
        }


    }
}
