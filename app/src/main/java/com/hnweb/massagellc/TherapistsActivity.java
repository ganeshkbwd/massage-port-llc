package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.TherapistMessageAdapter;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.TherapistsMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class TherapistsActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recyclerView;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int login, user_id;
    ArrayList<TherapistsMessage> therapistsMessageArrayList = new ArrayList<TherapistsMessage>();
    TextView noContactsTV;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapists);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);

        login = sharedPreferences.getInt("login", 0);
        Log.e("id", String.valueOf(login));
        user_id = sharedPreferences.getInt("user_id", 0);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        changeStatusBarColor();
        noContactsTV = (TextView) findViewById(R.id.noContactsTV);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        messageWebservice();

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {

            case R.id.viewMapBTN:
                intent = new Intent(TherapistsActivity.this, MapsActivity.class);
                startActivity(intent);
                finish();

                break;
            case R.id.myAppBTN:
                intent = new Intent(TherapistsActivity.this, MyAppointmentActivity.class);
                startActivity(intent);
                finish();
//                Toast.makeText(MapsActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                break;
            case R.id.myProfileBTN:
//                Toast.makeText(MapsActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                intent = new Intent(TherapistsActivity.this, CustomerProfileActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.msgBTN:
                Toast.makeText(TherapistsActivity.this, "You are currently on View Map", Toast.LENGTH_SHORT).show();

                break;
        }

    }

    public void messageWebservice() {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.getTherapists,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");


                            if (ConstantVal.MESSAGE_CODE == 1) {
                                noContactsTV.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                therapistsMessageArrayList.clear();
                                JSONArray jarr = jobj.getJSONArray("response");
                                for (int i = 0; i < jarr.length(); i++) {

                                    TherapistsMessage c = new TherapistsMessage();
                                    c.setBooking_id(jarr.getJSONObject(i).getInt("booking_id"));
                                    c.setBooking_type(jarr.getJSONObject(i).getString("booking_type"));
                                    c.setDate(jarr.getJSONObject(i).getString("date"));
                                    c.setHour(jarr.getJSONObject(i).getString("hour"));
                                    c.setMinute(jarr.getJSONObject(i).getString("minute"));
                                    c.setPeriod(jarr.getJSONObject(i).getString("period"));
                                    c.setCustomer_id(jarr.getJSONObject(i).getString("customer_id"));
                                    c.setTherapist_id(jarr.getJSONObject(i).getString("therapist_id"));
                                    c.setCreated_datetime(jarr.getJSONObject(i).getString("created_datetime"));
                                    c.setCustomer_address(jarr.getJSONObject(i).getString("customer_address"));
                                    c.setTherapist_name(jarr.getJSONObject(i).getString("therapist_name"));
                                    c.setProfile_photo(jarr.getJSONObject(i).getString("profile_photo"));
                                    c.setFull_address(jarr.getJSONObject(i).getString("full_address"));
                                    c.setAvg_rating(jarr.getJSONObject(i).getString("avg_rating"));
                                    c.setTotal_reviews(jarr.getJSONObject(i).getString("total_reviews"));

                                    therapistsMessageArrayList.add(c);


                                }
                                recyclerView.setAdapter(new TherapistMessageAdapter(TherapistsActivity.this, therapistsMessageArrayList));

                                ConstantVal.progressDialog.dismiss();


                            } else {
                                noContactsTV.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                ConstantVal.MESSAGE = jobj.getString("message");
                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(TherapistsActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistsActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("customer_id", String.valueOf(user_id));


                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (login == 1)
            getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_logout:
                Methodes.logout_fun(this);
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
