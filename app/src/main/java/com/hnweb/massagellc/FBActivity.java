package com.hnweb.massagellc;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


public class FBActivity extends AppCompatActivity implements FacebookCallback<LoginResult> {
    private CallbackManager callbackManager;
    private Bitmap profileImage;
    private String personId;
    private String strFirstName;
    private String strLastName;
    private Uri personPhotoUri;
    private String strEmailId;
    private Intent selectedImageResultIntent = null;
    Button loginButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_fb);
        keyHash();

//        loginButton = (Button) findViewById(R.id.loginBTN);
//        loginButton.setCompoundDrawables(null, null, null, null);
//        loginButton.setReadPermissions("user_friends");
//        loginButton.setReadPermissions("email");
//        loginButton.setText("");
//// loginButton.setActi(this);
//        loginButton.registerCallback(mCallbackManager, mFacebookCallback);
//        loginButton.setOnClickListener(this);

        //***** Facebook Integration *****//
        // Initialize the SDK before executing any other operations,
        // especially, if you're using Facebook UI elements.
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, this);
//        LoginManager.getInstance().setLoginBehavior(strEmailId)
        // Register the on click listener for the Facebook Sign Up button
//        findViewById(R.id.fb_login_button);


//
//        // My facebook Profile info
//        fbMyProfileButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                getFacebookProfileInfo();
//            }
//        });
//
//        // Post at Facebook Wall
//        fbPostToWallButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                facebookWallPost();
//
//            }
//        });
//
//        // Post at Facebook Wall
//        fbFriendButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                getFacebookFriends();
//
//            }
//        });
    }

    public void keyHash() {
        try {
            PackageInfo info;
            info = getPackageManager().getPackageInfo("com.hnweb.massagellc", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    @Override
    public void onSuccess(final LoginResult loginResult) {
        if (BuildConfig.DEBUG) {
            Log.d("TAG", "Facebook Sign Up Success...");
        }

        if (Profile.getCurrentProfile() == null) {
            ProfileTracker profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                    stopTracking();
                    processFacebookProfile(loginResult, newProfile);
                }
            };

        } else {
            processFacebookProfile(loginResult, Profile.getCurrentProfile());
        }

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {
        if (error instanceof FacebookAuthorizationException) {
            if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut();
            }
        }
        if (BuildConfig.DEBUG) {
            if (error != null && error.getCause() != null) {
                Log.d("TAG", "Facebook Sign Up Error...: " + error.getCause().toString());
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
//            if (requestCode == SELECT_FILE) {
            selectedImageResultIntent = data;
//                onSelectFromGalleryResult(data);
//            } else if (requestCode == REQUEST_CAMERA) {
            //checkPermissions();
//                onCaptureImageResult(data);
//            }
        }
    }

    private void processFacebookProfile(LoginResult loginResult, Profile profile) {
        personId = profile.getId();

        strFirstName = profile.getFirstName();
        strLastName = profile.getLastName();
        personPhotoUri = profile.getProfilePictureUri(500, 500);

        // Facebook Email address
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (BuildConfig.DEBUG) {
                            Log.v("TAG", response.toString());
                        }
                        try {
                            String name = object.getString("name");
//                            String email = profile.getString("email");
                            Toast.makeText(FBActivity.this, "Logged In", Toast.LENGTH_SHORT).show();
//                            Intent i = new Intent(this, MainActivity.class);
//                            startActivity(i);
//                            finish();
//
//                            strEmailId = object.getString("email");
//

                            if (BuildConfig.DEBUG)
                                Log.d("TG", "FirstName: " + strFirstName + ", LastName: " + strLastName + ", Email: " + strEmailId
                                        + ", personId: " + personId + ", personPhotoUri: " + personPhotoUri);
//
//                            // Set the Sign Up type
//                            type = GlobalConstants.TYPE_LOGIN_FACEBOOK;
//                            strPassword = "xyz";
//                            doRegister();

                        } catch (JSONException e) {
                            if (BuildConfig.DEBUG) e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }


    private void signUpFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }


}
