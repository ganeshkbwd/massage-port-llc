package com.hnweb.massagellc.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hnweb.massagellc.R;

import org.w3c.dom.Text;

/**
 * Created by neha on 11/11/2016.
 */
public class CustomersViewHolder extends RecyclerView.ViewHolder{

    public TextView customerNameTV;
    public Button replyBTN;

    public CustomersViewHolder(View itemView) {
        super(itemView);

        customerNameTV = (TextView) itemView.findViewById(R.id.customerNameTV);
        replyBTN = (Button) itemView.findViewById(R.id.replyBTN);

    }
}
