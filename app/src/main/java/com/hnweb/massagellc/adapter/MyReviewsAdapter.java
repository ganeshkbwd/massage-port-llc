package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.MyReviews;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by neha on 10/18/2016.
 */
public class MyReviewsAdapter extends BaseAdapter {
    Context context;
    ArrayList<MyReviews> mRList;

    public MyReviewsAdapter(Activity activity, ArrayList<MyReviews> mRList) {
        this.context = activity;
        this.mRList = mRList;
    }

    @Override
    public int getCount() {
        return mRList.size();
    }

    @Override
    public Object getItem(int position) {
        return mRList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mRList.indexOf(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Collections.reverse(mRList);
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.my_reviwes_item, null);
            holder = new ViewHolder();
            holder.customerNameTV = (TextView) convertView.findViewById(R.id.customerNameTV);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingRB);
            holder.replyBTN = (Button) convertView.findViewById(R.id.replyBTN);

            LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

            holder.commentTV = (TextView) convertView.findViewById(R.id.commentTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (!mRList.get(position).getCustomer_name().equalsIgnoreCase("null")) {
            holder.customerNameTV.setText(mRList.get(position).getCustomer_name());
            holder.ratingBar.setRating(Float.parseFloat(mRList.get(position).getStars()));
            holder.commentTV.setText(mRList.get(position).getComment());
        }

        holder.replyBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog settingsDialog = new Dialog(context);
                LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inf.inflate(R.layout.reply_dialog, null);
                settingsDialog.setContentView(view);
                settingsDialog.setCancelable(false);
                settingsDialog.setTitle("Reply Text");
                final EditText replyET = (EditText) view.findViewById(R.id.replyEd);
                Button submitBTN = (Button) view.findViewById(R.id.submitBTN);
                Button cancelBTN = (Button) view.findViewById(R.id.cancelBTN);
                submitBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (replyET.getText().toString().trim().length() > 0) {
                            if (CheckConnectivity.checkInternetConnection(context)) {
                                replyToReviews(replyET.getText().toString().trim(), mRList.get(position).getReview_id());
                                settingsDialog.dismiss();
                            } else {
                                CheckConnectivity.noNetMeg(context);
                            }


                        } else {
                            Toast.makeText(context, "Please enter reply.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                cancelBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        settingsDialog.dismiss();
                    }
                });


                settingsDialog.show();
            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView customerNameTV;
        RatingBar ratingBar;
        TextView commentTV;
        Button replyBTN;

    }

    private void replyToReviews(final String reply, final int review_id) {
        ConstantVal.progressDialog = new ProgressDialog(context);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.replytocustomer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {

                                Toast.makeText(context, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();

                            } else {

                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(context, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("review_id", String.valueOf(review_id));
                params.put("reply_text", reply);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);
    }
}
