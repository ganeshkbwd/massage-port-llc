package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.pojo.MyScheduleAppointments;
import com.hnweb.massagellc.pojo.TherapistConfirmAppointments;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by neha on 10/15/2016.
 */
public class TherapistAppointmentsAdapter extends BaseAdapter {
    ArrayList<TherapistConfirmAppointments> msaList;
    Context context;

    public TherapistAppointmentsAdapter(Activity activity, ArrayList<TherapistConfirmAppointments> msaList1) {
        this.context = activity;
        this.msaList = msaList1;
    }

    @Override
    public int getCount() {
        return msaList.size();
    }

    @Override
    public Object getItem(int position) {
        return msaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return msaList.indexOf(position);
    }

    @Override
    public View getView(int position, View myView, ViewGroup parent) {
        Collections.reverse(msaList);
        View convertView = myView;
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.therapist_appointment_adapter, null);
            holder = new ViewHolder();


            holder.fullnameTV = (TextView) convertView.findViewById(R.id.customerNameTV);
            holder.appDatetimeTV = (TextView) convertView.findViewById(R.id.appDateTV);
            holder.addressTV = (TextView) convertView.findViewById(R.id.addressTV);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//
        String hour, minute;
        if (Integer.parseInt(msaList.get(position).getHour()) < 10) {
            hour = "0" + Integer.parseInt(msaList.get(position).getHour());
        } else {
            hour = msaList.get(position).getHour() + "";
        }

        if (Integer.parseInt(msaList.get(position).getMinute()) < 10) {
            minute = "0" + Integer.parseInt(msaList.get(position).getMinute());
        } else {
            minute = msaList.get(position).getMinute() + "";
        }
//        if (isPastDate(msaList.get(position).getDate())) {
//            holder.fullnameTV.setBackgroundColor(Color.GRAY);
//        }
//
//
        holder.appDatetimeTV.setText(msaList.get(position).getDate() + ", " + hour + " : " + minute + " " + msaList.get(position).getPeriod());
//
        holder.addressTV.setText(msaList.get(position).getCustomer_address());
        Log.e("MSA",msaList.get(position).getFull_address());
        holder.fullnameTV.setText(msaList.get(position).getCustomerName());

        return convertView;
    }

    private class ViewHolder {

        TextView fullnameTV;
        TextView appDatetimeTV;
        TextView addressTV;


    }

    private boolean isPastDate(String date) {

        Date givenDate = null, current = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        String currentDate = sdf.format(calendar.getTime());

//        try {
//            givenDate = sdf.parse(date);
//            current = sdf.parse(currentDate);
//
////            return "true";
//        } catch (ParseException e) {
//            e.printStackTrace();
////            return "false";
//        }
        String[] myDate = date.split("-");
        String[] today = currentDate.split("-");
        if (((Integer.parseInt(myDate[0].toString())) - (Integer.parseInt(today[0].toString())) > -1)) {

            if ((Integer.parseInt(myDate[1].toString()) - Integer.parseInt(today[1].toString())) > -1) {

                if ((Integer.parseInt(myDate[2].toString()) - Integer.parseInt(today[2].toString())) > -1) {
                    return true;

                } else {
                    return false;
                }

            } else {
                return false;
            }
        } else {
            return false;
        }

//        if (givenDate.before(current) == true) {
////            Log.e("PAST", "TRUE");
//            return true;
//        } else {
//
////            Log.e("PAST", "False");
//            return false;
//        }
//        return givenDate.before(current);

    }
}

