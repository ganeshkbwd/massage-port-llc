package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hnweb.massagellc.MessageToCustomerActivity;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.pojo.TherapistToTherapist;

import java.util.ArrayList;

/**
 * Created by neha on 12/26/2016.
 */

public class TherapisttoTherapistAdapter extends RecyclerView.Adapter<TTTViewHolder> {

    Context context;
    ArrayList<TherapistToTherapist> therapistsArrayList;

    public TherapisttoTherapistAdapter(Activity activity, ArrayList<TherapistToTherapist> therapistsArrayList) {
        this.context = activity;
        this.therapistsArrayList = therapistsArrayList;

    }

    @Override
    public TTTViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.customers_list_item, null);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));
        TTTViewHolder rcv = new TTTViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(TTTViewHolder holder, final int position) {
        holder.customerNameTV.setText(therapistsArrayList.get(position).getTherapist_name());
        holder.replyBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessageToCustomerActivity.class);
                intent.putExtra("ID", therapistsArrayList.get(position).getTo_user_id());
                intent.putExtra("NAME", therapistsArrayList.get(position).getTherapist_name());
//                intent.putExtra("TIME", therapistsArrayList.get(position).getCreated_datetime());

                context.startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return therapistsArrayList.size();
    }
}
