package com.hnweb.massagellc.adapter;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hnweb.massagellc.R;

/**
 * Created by neha on 10/26/2016.
 */
public class ReviewViewHolder extends RecyclerView.ViewHolder {
    TextView customerNameTV;
    RatingBar ratingBar;
    TextView commentTV;

    public ReviewViewHolder(View itemView) {
        super(itemView);

         customerNameTV = (TextView) itemView.findViewById(R.id.customerNameTV);

         ratingBar= (RatingBar) itemView.findViewById(R.id.ratingRB);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
         commentTV = (TextView) itemView.findViewById(R.id.customerCommentTV);

    }
}
