package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.pojo.CustomerSideReviews;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by neha on 9/29/2016.
 */
public class CustomerSideReviewsAdapter extends BaseAdapter {
    Context context;
    ArrayList<CustomerSideReviews> csrList;
    ArrayList<CustomerSideReviews> newCsrList;



    public CustomerSideReviewsAdapter(Activity activity, ArrayList<CustomerSideReviews> csrList) {

        this.context = activity;

        this.csrList = new ArrayList<CustomerSideReviews>(csrList);


    }

    @Override
    public int getCount() {
        return csrList.size();
    }

    @Override
    public Object getItem(int position) {
        return csrList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return csrList.indexOf(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Collections.reverse(csrList);
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.reviews_list_item, null);
            holder = new ViewHolder();
            holder.customerNameTV = (TextView) convertView.findViewById(R.id.customerNameTV);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingRB);

            LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

            holder.commentTV = (TextView) convertView.findViewById(R.id.customerCommentTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (!csrList.get(position).getCustomer_name().equalsIgnoreCase("null")){
            holder.customerNameTV.setText(csrList.get(position).getCustomer_name());
            holder.ratingBar.setRating(Float.parseFloat(csrList.get(position).getStars()));
            holder.commentTV.setText(csrList.get(position).getComment());
        }



        return convertView;
    }

    private class ViewHolder {
        TextView customerNameTV;
        RatingBar ratingBar;
        TextView commentTV;

    }


}
