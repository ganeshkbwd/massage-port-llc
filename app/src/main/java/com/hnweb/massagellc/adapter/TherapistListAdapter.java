package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.pojo.TherapistData;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by neha on 9/22/2016.
 */
public class TherapistListAdapter extends BaseAdapter {
    Context context;
    List<TherapistData> tdList;


    public TherapistListAdapter(Activity activity, List<TherapistData> therapistDataList) {
        this.context = activity;
        this.tdList = therapistDataList;
    }

    @Override
    public int getCount() {
        return tdList.size();
    }

    @Override
    public Object getItem(int position) {
        return tdList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tdList.indexOf(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.therapist_list_item_layout, null);
            holder = new ViewHolder();
            holder.profileIV = (ImageView) convertView.findViewById(R.id.profileIV);
            holder.fullnameTV = (TextView) convertView.findViewById(R.id.fullNameTV);
            holder.avialstatusTV = (TextView) convertView.findViewById(R.id.statusTV);
            holder.ratingTV = (TextView) convertView.findViewById(R.id.ratingTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (!tdList.get(position).getProfile_photo().equals(""))
            Picasso.with(context).load(tdList.get(position).getProfile_photo()).into(holder.profileIV);
        holder.fullnameTV.setText(tdList.get(position).getFull_name());
        holder.avialstatusTV.setText("Avialable Status : " + tdList.get(position).getAvailable_status());
        holder.ratingTV.setText("Rating : " + tdList.get(position).getAvg_rating());

        return convertView;
    }

    private class ViewHolder {
        ImageView profileIV;
        TextView fullnameTV;
        TextView avialstatusTV;
        TextView ratingTV;

    }
}
