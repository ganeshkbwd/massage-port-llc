package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.pojo.AvailableTimings;

import java.util.ArrayList;

/**
 * Created by neha on 9/24/2016.
 */
public class TimingsAdapter extends BaseAdapter {
    Context context;
    ArrayList<AvailableTimings> availableTimings;
    int listIndex = -1;


    public TimingsAdapter(ArrayList<AvailableTimings> availableTimings, Activity activity) {
        this.context = activity;
        this.availableTimings = availableTimings;
    }

    @Override
    public int getCount() {
        return availableTimings.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;


        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.appoitment_schedule_item_layout, null);
            holder = new ViewHolder();
            holder.appScheduleLL = (LinearLayout) convertView.findViewById(R.id.appScheduleLL);
            holder.radioButton = (RadioButton) convertView.findViewById(R.id.radioButton);
            holder.timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            holder.avialstatusTV = (TextView) convertView.findViewById(R.id.availTV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.radioButton.setChecked(position == ConstantVal.selectedPosition);
        holder.radioButton.setTag(position);

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConstantVal.selectedPosition = (Integer) view.getTag();
                notifyDataSetChanged();
            }
        });

        String hours, minutes;


        if (availableTimings.get(position).getHour() < 10) {
            hours = "0" + availableTimings.get(position).getHour();
        } else {
            hours = availableTimings.get(position).getHour() + "";
        }
        if (availableTimings.get(position).getMinute() < 10) {
            minutes = "0" + availableTimings.get(position).getMinute();
        } else {
            minutes = availableTimings.get(position).getMinute() + "";
        }

        holder.timeTV.setText(hours + " : " + minutes + "  " + availableTimings.get(position).getPeriod());
        holder.avialstatusTV.setText(availableTimings.get(position).getStatus());
        if (availableTimings.get(position).getStatus().equalsIgnoreCase("Booked")) {
            holder.radioButton.setEnabled(false);
            holder.radioButton.setChecked(false);
            holder.timeTV.setTextColor(Color.RED);
            holder.avialstatusTV.setTextColor(Color.RED);
        } else {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ConstantVal.selectedPosition = (int) holder.radioButton.getTag();
                    notifyDataSetChanged();
                }
            });
        }


        return convertView;
    }

    private class ViewHolder {
        RadioButton radioButton;
        TextView timeTV;
        TextView avialstatusTV;
        LinearLayout appScheduleLL;

    }
}
