package com.hnweb.massagellc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hnweb.massagellc.MessageToCustomerActivity;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.pojo.Customers;

import java.util.ArrayList;

/**
 * Created by neha on 11/11/2016.
 */
public class CustomersAdapter extends RecyclerView.Adapter<CustomersViewHolder> {
    Context context;
    ArrayList<Customers> customersArrayList;

    public CustomersAdapter(FragmentActivity activity, ArrayList<Customers> customersArrayList) {
        this.context = activity;
        this.customersArrayList = customersArrayList;

    }

    @Override
    public CustomersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.customers_list_item, null);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));
        CustomersViewHolder rcv = new CustomersViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(CustomersViewHolder holder, final int position) {

        holder.customerNameTV.setText(customersArrayList.get(position).getCustomer_name());
        holder.replyBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,MessageToCustomerActivity.class);
                intent.putExtra("ID",customersArrayList.get(position).getCustomer_id());
                intent.putExtra("NAME",customersArrayList.get(position).getCustomer_name());
                intent.putExtra("TIME", customersArrayList.get(position).getCreated_datetime());

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return customersArrayList.size();
    }


}
