package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.pojo.MyScheduleAppointments;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by neha on 9/28/2016.
 */
public class MyAppointmentAdapter extends BaseAdapter {
    ArrayList<MyScheduleAppointments> msaList;
    Context context;

    public MyAppointmentAdapter(Activity activity, ArrayList<MyScheduleAppointments> msaList) {
        this.context = activity;
        this.msaList = msaList;
    }

    @Override
    public int getCount() {
        return msaList.size();
    }

    @Override
    public Object getItem(int position) {
        return msaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return msaList.indexOf(position);
    }

    @Override
    public View getView(int position, View myView, ViewGroup parent) {
        Collections.reverse(this.msaList);
        View convertView = myView;
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.my_appointment_list_item, null);
            holder = new ViewHolder();

            holder.profileIV = (ImageView) convertView.findViewById(R.id.therapist_profileIV);
            holder.fullnameTV = (TextView) convertView.findViewById(R.id.therapistNameTV);
            holder.appDatetimeTV = (TextView) convertView.findViewById(R.id.appDateTV);
            holder.addressTV = (TextView) convertView.findViewById(R.id.addressTV);
            holder.viewDetailBTN = (Button) convertView.findViewById(R.id.viewDetailsBTN);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingTV);
            LayerDrawable stars = (LayerDrawable) holder.ratingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (!msaList.get(position).getProfile_photo().toString().equals("")) {
            Picasso.with(context).load(msaList.get(position).getProfile_photo()).into(holder.profileIV);
        }

//
        String hour, minute;
        if (Integer.parseInt(msaList.get(position).getHour()) < 10) {
            hour = "0" + Integer.parseInt(msaList.get(position).getHour());
        } else {
            hour = msaList.get(position).getHour() + "";
        }

        if (Integer.parseInt(msaList.get(position).getMinute()) < 10) {
            minute = "0" + Integer.parseInt(msaList.get(position).getMinute());
        } else {
            minute = msaList.get(position).getMinute() + "";
        }
//        if (isPastDate(msaList.get(position).getDate())) {
//            holder.fullnameTV.setBackgroundColor(Color.GRAY);
//        }


            holder.appDatetimeTV.setText(msaList.get(position).getDate() + ", " + hour + " : " + minute + " " + msaList.get(position).getPeriod());
        holder.fullnameTV.setText(msaList.get(position).getTherapist_name());
        holder.addressTV.setText(msaList.get(position).getFull_address());
        holder.ratingBar.setRating(Float.parseFloat(msaList.get(position).getAvg_rating()));
        holder.viewDetailBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Under Development....", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(context, MapsActivity.class);
////                intent.putExtra("ComeFrom", "Review");
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                context.startActivity(intent);

//                Toast.makeText(context, "Under Development...", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    private class ViewHolder {

        ImageView profileIV;
        TextView fullnameTV;
        TextView appDatetimeTV;
        TextView addressTV;
        Button viewDetailBTN;
        RatingBar ratingBar;

    }


}
