package com.hnweb.massagellc.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hnweb.massagellc.R;

/**
 * Created by neha on 11/11/2016.
 */
public class TherapistMessageViewHolder extends RecyclerView.ViewHolder {
    public TextView customerNameTV;
    public Button replyBTN;
//    public ImageView therapist_profileIV;

    public TherapistMessageViewHolder(View itemView) {
        super(itemView);

        customerNameTV = (TextView) itemView.findViewById(R.id.customerNameTV);
        replyBTN = (Button) itemView.findViewById(R.id.replyBTN);
//        therapist_profileIV = (ImageView) itemView.findViewById(R.id.therapist_profileIV);
    }
}
