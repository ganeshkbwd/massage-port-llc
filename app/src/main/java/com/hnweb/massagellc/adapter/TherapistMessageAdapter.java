package com.hnweb.massagellc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hnweb.massagellc.MessageToCustomerActivity;
import com.hnweb.massagellc.MessageToTherapistActivity;
import com.hnweb.massagellc.R;
import com.hnweb.massagellc.TherapistsActivity;
import com.hnweb.massagellc.pojo.TherapistsMessage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by neha on 11/11/2016.
 */
public class TherapistMessageAdapter extends RecyclerView.Adapter<TherapistMessageViewHolder> {

    Context context;
    ArrayList<TherapistsMessage> therapistsMessageArrayList;

    public TherapistMessageAdapter(TherapistsActivity therapistsActivity, ArrayList<TherapistsMessage> therapistsMessageArrayList) {
        this.context = therapistsActivity;
        this.therapistsMessageArrayList = therapistsMessageArrayList;
    }

    @Override
    public TherapistMessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.therapist_msg_list_item, null);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));
        TherapistMessageViewHolder rcv = new TherapistMessageViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(TherapistMessageViewHolder holder, final int position) {
        holder.customerNameTV.setText(therapistsMessageArrayList.get(position).getTherapist_name());
        holder.replyBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MessageToTherapistActivity.class);
                intent.putExtra("ID", therapistsMessageArrayList.get(position).getTherapist_id());
                intent.putExtra("NAME", therapistsMessageArrayList.get(position).getTherapist_name());
//                intent.putExtra("IMAGE", therapistsMessageArrayList.get(position).getProfile_photo());
                intent.putExtra("TIME", therapistsMessageArrayList.get(position).getCreated_datetime());

                context.startActivity(intent);

            }
        });
//        Picasso.with(context).load(therapistsMessageArrayList.get(position).getProfile_photo()).into(holder.therapist_profileIV);
    }

    @Override
    public int getItemCount() {
        return therapistsMessageArrayList.size();
    }
}
