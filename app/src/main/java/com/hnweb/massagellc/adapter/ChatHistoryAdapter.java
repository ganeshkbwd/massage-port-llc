package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.pojo.ChatHistory;

import java.util.ArrayList;

/**
 * Created by neha on 11/11/2016.
 */
public class ChatHistoryAdapter extends RecyclerView.Adapter<ChatHistoryViewHolder> {
    Context context;
    ArrayList<ChatHistory> chatHistoryArrayList;
    int user_id;
    String name;

    public ChatHistoryAdapter(Activity activity, ArrayList<ChatHistory> chatHistoryArrayList, int user_id, String name) {
        this.context = activity;
        this.chatHistoryArrayList = chatHistoryArrayList;
        this.user_id = user_id;
        this.name = name;
    }

    @Override
    public ChatHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, null);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));
        ChatHistoryViewHolder rcv = new ChatHistoryViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ChatHistoryViewHolder holder, int position) {

        holder.chatMsg.setText(chatHistoryArrayList.get(position).getMessage());
        holder.timeTV.setText(chatHistoryArrayList.get(position).getCreated_datetime());
        if (user_id == Integer.parseInt(chatHistoryArrayList.get(position).getFrom_userid())) {
            holder.nameTV.setText("me");
        }else {
            holder.nameTV.setText(name);
        }

    }

    @Override
    public int getItemCount() {
        return chatHistoryArrayList.size();
    }
}
