package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.hnweb.massagellc.R;

import java.util.ArrayList;

/**
 * Created by neha on 9/28/2016.
 */
public class CustProfileAdapter extends BaseAdapter {
    ArrayList<String> cpList;
    ArrayList<String> titlesList;

    Context context;

    public CustProfileAdapter(Activity activity, ArrayList<String> cpList, ArrayList<String> titlesList) {
        this.context = activity;
        this.cpList = cpList;
        this.titlesList = titlesList;

    }

    @Override
    public int getCount() {
        return cpList.size();
    }

    @Override
    public Object getItem(int position) {
        return cpList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cpList.indexOf(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.customer_profile_list_item, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.titleTV);
            holder.description = (EditText) convertView.findViewById(R.id.descriptionTV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(titlesList.get(position).toString());
        Log.e("title", titlesList.get(position).toString());
        holder.description.setText(cpList.get(position).toString().replaceAll("%20", " "));
        Log.e("title", cpList.get(position).toString());

        return convertView;
    }

    private class ViewHolder {
        TextView title;
        EditText description;

    }
}
