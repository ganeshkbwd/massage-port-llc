package com.hnweb.massagellc.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hnweb.massagellc.R;

/**
 * Created by neha on 11/11/2016.
 */
public class ChatHistoryViewHolder extends RecyclerView.ViewHolder {
    public TextView chatMsg,timeTV,nameTV;

    public ChatHistoryViewHolder(View itemView) {
        super(itemView);
        chatMsg = (TextView) itemView.findViewById(R.id.msgTV);
        timeTV = (TextView) itemView.findViewById(R.id.timeTV);
        nameTV = (TextView) itemView.findViewById(R.id.nameTV);

    }
}
