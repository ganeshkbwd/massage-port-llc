package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.helper.ConstantVal;

import java.util.ArrayList;

/**
 * Created by neha on 10/7/2016.
 */
public class SetAppointmentTimingsAdapter extends BaseAdapter {
    ArrayList<String> sATList;
    Context context;

    public SetAppointmentTimingsAdapter(ArrayList<String> setAppointmentTimeList, Activity activity) {
        this.context = activity;
        this.sATList = setAppointmentTimeList;
    }

    @Override
    public int getCount() {
        return sATList.size();
    }

    @Override
    public Object getItem(int position) {
        return sATList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return sATList.indexOf(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;


        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.set_appointment_time_item, null);
            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.checkBox.setText(sATList.get(position).toString());
        if (ConstantVal.avialAllreadyTimeList.size() != 0) {

            if (ConstantVal.avialAllreadyTimeList.contains(sATList.get(position).toString())) {
                holder.checkBox.setChecked(true);
                holder.checkBox.setEnabled(false);
            } else {
                holder.checkBox.setChecked(false);
                holder.checkBox.setEnabled(true);
            }
        }


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (!ConstantVal.avialAllreadyTimeList.contains(holder.checkBox.getText().toString()))
                        ConstantVal.avialAllreadyTimeList.add(holder.checkBox.getText().toString());
                } else {
                    ConstantVal.avialAllreadyTimeList.remove(holder.checkBox.getText().toString());
                }
            }
        });

        return convertView;
    }

    private class ViewHolder {
        CheckBox checkBox;

    }
}
