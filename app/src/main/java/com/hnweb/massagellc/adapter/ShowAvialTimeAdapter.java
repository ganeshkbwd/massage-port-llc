package com.hnweb.massagellc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;

import com.hnweb.massagellc.R;

import java.util.ArrayList;

/**
 * Created by neha on 10/18/2016.
 */
public class ShowAvialTimeAdapter extends BaseAdapter {
    ArrayList<String> sATList;
    Context context;


    public ShowAvialTimeAdapter(ArrayList<String> showAvialTimeList, Activity activity) {
        this.context = activity;
        this.sATList = showAvialTimeList;

    }

    @Override
    public int getCount() {
        return sATList.size();
    }

    @Override
    public Object getItem(int position) {
        return sATList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return sATList.indexOf(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.set_appointment_time_item, null);
            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.checkBox.setText(sATList.get(position).toString());
        holder.checkBox.setChecked(true);
        holder.checkBox.setClickable(false);

        return convertView;
    }

    private class ViewHolder {
        CheckBox checkBox;

    }
}
