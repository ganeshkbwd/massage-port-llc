package com.hnweb.massagellc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hnweb.massagellc.R;
import com.hnweb.massagellc.ReviewCustomerSideActivity;
import com.hnweb.massagellc.pojo.CustomerSideReviews;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by neha on 10/26/2016.
 */
public class CustomerReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder> {
    Context context;
    ArrayList<CustomerSideReviews> csrList;


    public CustomerReviewAdapter(ReviewCustomerSideActivity reviewCustomerSideActivity, ArrayList<CustomerSideReviews> csrList) {
        this.context = reviewCustomerSideActivity;
        this.csrList = csrList;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_list_item, null);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        layoutView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));
        ReviewViewHolder rcv = new ReviewViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        Collections.reverse(csrList);
        holder.customerNameTV.setText(csrList.get(position).getCustomer_name());
        holder.ratingBar.setRating(Float.parseFloat(csrList.get(position).getStars()));
        holder.commentTV.setText(csrList.get(position).getComment());
    }

    @Override
    public int getItemCount() {
        return csrList.size();
    }
}
