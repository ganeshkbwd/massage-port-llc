package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.adapter.TherapistListAdapter;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.TherapistData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AllTherapistListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView therapistLV;
    TherapistListAdapter therapistListAdapter;
    List<TherapistData> therapistDataList = new ArrayList<TherapistData>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_therapist_list);

        changeStatusBarColor();
        therapistLV = (ListView) findViewById(R.id.allTherapistLV);


        getAllTherapist();

        therapistLV.setOnItemClickListener(this);


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle b = new Bundle();
        b.putSerializable("List", (Serializable) therapistDataList);
        b.putInt("Position", position);
        Intent intent = new Intent(this, TherapistProfileActivity.class);
        intent.putExtras(b);

        startActivity(intent);

    }

    public void getAllTherapist() {
        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Sending ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Urls.getAllTherapist,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                JSONArray jsonArray = jobj.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    TherapistData td = new TherapistData();
                                    td.setUser_id(jsonArray.getJSONObject(i).getInt("user_id"));
                                    td.setUser_type(jsonArray.getJSONObject(i).getString("user_type"));
                                    td.setFull_name(jsonArray.getJSONObject(i).getString("full_name"));
                                    td.setEmail_address(jsonArray.getJSONObject(i).getString("email_address"));
                                    td.setUsername(jsonArray.getJSONObject(i).getString("username"));
                                    td.setPassword(jsonArray.getJSONObject(i).getString("password"));
                                    td.setLatitude(jsonArray.getJSONObject(i).getString("latitude"));
                                    td.setLongitude(jsonArray.getJSONObject(i).getString("longitude"));
                                    td.setFull_address(jsonArray.getJSONObject(i).getString("full_address"));
                                    td.setExpired_datetime(jsonArray.getJSONObject(i).getString("expired_datetime"));
                                    td.setProfile_photo(jsonArray.getJSONObject(i).getString("profile_photo"));
                                    td.setSpeciality(jsonArray.getJSONObject(i).getString("speciality"));
                                    td.setRates(jsonArray.getJSONObject(i).getString("rates"));
                                    td.setLast_update(jsonArray.getJSONObject(i).getString("last_update"));
                                    td.setCreated_datetime(jsonArray.getJSONObject(i).getString("created_datetime"));
                                    td.setAvailable_status(jsonArray.getJSONObject(i).getString("available_status"));
                                    td.setAvg_rating(jsonArray.getJSONObject(i).getString("avg_rating"));
                                    td.setTotal_reviews(jsonArray.getJSONObject(i).getString("total_reviews"));
                                    therapistDataList.add(td);

                                }

                                Log.e("LIST", therapistDataList.toString());
                                therapistListAdapter = new TherapistListAdapter(AllTherapistListActivity.this, therapistDataList);
                                therapistLV.setAdapter(therapistListAdapter);

                                ConstantVal.progressDialog.dismiss();
//                                settingsDialog.dismiss();
//                                Toast.makeText(context, ConstantVal.RESPONSE, Toast.LENGTH_SHORT).show();

                            } else {
                                ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                                ConstantVal.progressDialog.dismiss();

                                Toast.makeText(AllTherapistListActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(AllTherapistListActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);
    }
}
