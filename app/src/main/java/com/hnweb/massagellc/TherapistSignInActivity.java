package com.hnweb.massagellc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

public class TherapistSignInActivity extends AppCompatActivity implements View.OnClickListener {
    EditText email_idET, passET;
    String email, pass;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    int upload;
    int therapist_id;
    String full_name, email_address, username, password, profile_photo, speciality, rates, available_status;
    private CallbackManager mCallbackManager;
    private Profile profile;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;
    double latitude, longitude;
    Geocoder geocoder;
    List<Address> addresses;
    String address = "", city = "", state, country = "", postalCode, subLocality;
    private static final String TWITTER_KEY = "bbsYIzjpNOUsfbUwOHFrhNG5V";
    private static final String TWITTER_SECRET = "f4GYWnovWkA8XtrDimTfsw1VVMy9P8oNqXSQqrPBJxkGW2gGru";
    //Twitter Login Button
    TwitterLoginButton twitterLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fb_tw_init_sdk();

        setContentView(R.layout.activity_therapist_sign_in);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        upload = sharedPreferences.getInt("upload", 0);

        email_idET = (EditText) findViewById(R.id.userNameET);
        passET = (EditText) findViewById(R.id.passET);
        changeStatusBarColor();
        fbadd();
        twitInit();
    }

    public void fb_tw_init_sdk(){
        FacebookSdk.sdkInitialize(getApplicationContext());

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.signupBTN:
                intent = new Intent(this, TherapistSignUpActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.forgot_passBTN:
                forgotPasswordDialog();
                break;
            case R.id.loginBTN:
                email = email_idET.getText().toString().trim();
                pass = passET.getText().toString().trim();


                if (email.length() > 0) {
                    if (pass.length() > 0) {
                        callSignInWebservice(email, pass);
                    } else {
                        passET.setError("Please enter password.");
                    }
                } else {
                    email_idET.setError("Please enter username.");
                }

                break;
        }

    }

    public void forgotPasswordDialog() {
        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.forgot_password
                , null));
        settingsDialog.setCancelable(true);
        final EditText fppassTV = (EditText) settingsDialog.findViewById(R.id.fp_email_idET);
        Button submitBTN = (Button) settingsDialog.findViewById(R.id.fpsubmitBTN);
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fppassTV.getText().toString().trim().length() > 0 && Patterns.EMAIL_ADDRESS.matcher(fppassTV.getText().toString().trim()).matches()) {
                    String email = fppassTV.getText().toString().trim();
                    if (CheckConnectivity.checkInternetConnection(TherapistSignInActivity.this)) {
                        callForgotPasswordWebService(email, settingsDialog);
                    } else {
                        CheckConnectivity.noNetMeg(TherapistSignInActivity.this);
                    }


//                    Toast.makeText(SignInActivity.this, "Under Development ....", Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(TherapistSignInActivity.this, "Please enter your correct email id", Toast.LENGTH_SHORT).show();
                }
            }
        });

        settingsDialog.show();
    }

    public void callForgotPasswordWebService(final String email, final Dialog settingsDialog) {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.forgotPassword,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                ConstantVal.MESSAGE = jobj.getString("message");
                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();
                                settingsDialog.dismiss();

                            } else {

                                ConstantVal.progressDialog.dismiss();
                                Toast.makeText(TherapistSignInActivity.this, "Your entered email id not registered.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistSignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("email_address", email);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public void callSignInWebservice(final String username, final String pass) {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.signin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String email_address = jobj.getString("email_address");
                                String registerdLat = jobj.getString("latitude");
                                String registerdLong = jobj.getString("longitude");
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putInt("login", 2);
                                editor.putInt("user_id", user_id);
                                editor.putString("full_name", full_name);
                                editor.putString("email_address", email_address);
                                editor.putString("registerdLat", registerdLat);
                                editor.putString("registerdLong", registerdLong);
                                editor.putString("loginWith", "No");
                                editor.commit();

                                therapistDetailsWebService(user_id);
//                                Intent intent = new Intent(TherapistSignInActivity.this, TherapistTabsActivity.class);
//                                startActivity(intent);
//                                finish();
                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();

                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistSignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_type", ConstantVal.USER_TYPE);
                params.put("username", username);
                params.put("password", pass);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    public void therapistDetailsWebService(final int user_id) {


        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.therapistDetails,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {

//
                                therapist_id = jobj.getInt("user_id");
                                full_name = jobj.getString("full_name");
                                email_address = jobj.getString("email_address");
                                username = jobj.getString("username");
                                password = jobj.getString("password");
                                profile_photo = jobj.getString("profile_photo");
                                speciality = jobj.getString("speciality");
                                rates = jobj.getString("rates");
                                available_status = jobj.getString("available_status");
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putString("profile_photo", profile_photo);
                                editor.commit();
                                if (!profile_photo.equalsIgnoreCase("")) {
                                    Intent intent = new Intent(TherapistSignInActivity.this, TherapistTabsActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(TherapistSignInActivity.this, TherapistUploadProfileActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                                ConstantVal.progressDialog.dismiss();
                            } else {
                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistSignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("therapist_id", String.valueOf(user_id));
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    /////////// Facebook //////////////


    public void fbadd() {
        //        callbackManager = CallbackManager.Factory.create();
        /////////////////////////////////////////
        keyHash();
        mCallbackManager = CallbackManager.Factory.create();
        //   LoginManager.getInstance().logOut();
        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {

            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                //  textView.setText(displayMessage(profile1));
                // System.out.println(displayMessage(profile1));
            }
        };

        tokenTracker.startTracking();
        profileTracker.startTracking();


        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setCompoundDrawables(null, null, null, null);
        loginButton.setReadPermissions("user_friends");
        loginButton.setReadPermissions("email");
        loginButton.setText("");
        // loginButton.setActi(this);
        loginButton.registerCallback(mCallbackManager, mFacebookCallback);
        //////////////////////////////////////////////
    }

    public void keyHash() {
        try {
            PackageInfo info;
            info = getPackageManager().getPackageInfo("com.hnweb.massagellc", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            profile = Profile.getCurrentProfile();


            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.v("LoginActivity Response ", response.toString());
                            System.out.println("arshres" + response.toString());

                            try {
                                String id = object.getString("id");
                                String Name = object.getString("name");

                                String FEmail = object.getString("email");
                                Log.v("Email = ", id + "  " + FEmail + "  " + Name);
                                // Toast.makeText(getApplicationContext(), "Name " + Name+FEmail, Toast.LENGTH_LONG).show();
                                if (CheckConnectivity.checkInternetConnection(TherapistSignInActivity.this)) {

                                    fbSignInWebservice(id, Name, FEmail);
                                } else {
                                    CheckConnectivity.noNetMeg(TherapistSignInActivity.this);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();


            //textView.setText(displayMessage(profile));
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 140) {
            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        //   textView.setText(displayMessage(profile));
    }

    @Override
    public void onStop() {
        super.onStop();
        profileTracker.stopTracking();
        tokenTracker.stopTracking();
    }

    private String displayMessage(Profile profile) {
        StringBuilder stringBuilder = new StringBuilder();
        if (profile != null) {
            stringBuilder.append("Logged In " + profile.getFirstName() + profile.getProfilePictureUri(100, 100));
            Toast.makeText(getApplicationContext(), "Start Playing with the data " + profile.getFirstName(), Toast.LENGTH_SHORT).show();
        } else {
            stringBuilder.append("You are not logged in");
        }
        return stringBuilder.toString();
    }

    public void fbSignInWebservice(final String id, final String username, final String email) {

        latitude = Methodes.myLocation(TherapistSignInActivity.this).getLatitude();
        longitude = Methodes.myLocation(TherapistSignInActivity.this).getLongitude();
        final String address = getAddressFromLatLong(latitude, longitude);

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.fbLoginUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String email_address = jobj.getString("email_address");
                                String registerdLat = jobj.getString("latitude");
                                String registerdLong = jobj.getString("longitude");
                                SharedPreferences.Editor editor = sharedPreferences.edit();
//
                                editor.putInt("login", 2);
                                editor.putInt("user_id", user_id);
                                editor.putString("full_name", full_name);
                                editor.putString("email_address", email_address);
                                editor.putString("registerdLat", registerdLat);
                                editor.putString("registerdLong", registerdLong);
                                editor.putString("loginWith", "FB");
                                editor.commit();
//
                                therapistDetailsWebService(user_id);
////                                Intent intent = new Intent(TherapistSignInActivity.this, TherapistTabsActivity.class);
////                                startActivity(intent);
////                                finish();
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();

                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistSignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("full_name", username);
                params.put("email_address", email);
                params.put("facebook_user_id", id);
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));
                params.put("full_address", address);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    public String getAddressFromLatLong(double lattitude, double longitude) {
        String fulladdress = "";
        geocoder = new Geocoder(TherapistSignInActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lattitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            subLocality = addresses.get(0).getSubLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        fulladdress = address + "," + subLocality + "," + city + "," + state + "," + country;
        return fulladdress;
    }


    public void twitInit() {
        //Initializing twitter login button
        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);

        //Adding callback to the button
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                //If login succeeds passing the Calling the login method and passing Result object
                Log.e("Twit Result", result.toString());


                login(result);
            }

            @Override
            public void failure(TwitterException exception) {
                //If failure occurs while login handle it here
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });
    }

    //The login function accepting the result object
    public void login(Result<TwitterSession> result) {

        //Creating a twitter session with result's data
        final TwitterSession session = result.data;


        //Getting the username from session
        final String username = session.getUserName();
        final TwitterAuthToken authToken = session.getAuthToken();

        String token = authToken.token;
        String secret = authToken.secret;
        Log.e("SESSION", username + "   " + authToken);

        getUserData(session, username);

    }


    public void getUserData(TwitterSession session, final String username) {
        Call<User> userResult = Twitter.getApiClient(session).getAccountService().verifyCredentials(true, false);
        userResult.enqueue(new Callback<User>() {
            @Override
            public void success(Result<User> result) {
                User user = result.data;
                String twitterImage = user.profileImageUrl;

                try {
                    System.out.println("{" + " id : " + user.idStr + " name : " + user.name + " followers : " + String.valueOf(user.followersCount) + " createdAt : " + user.createdAt + " username :" + username + "}");
                    Log.d("id", user.idStr);
//                    Log.d("email",user.idStr);
                    Log.d("name", user.name);
//                    Log.d("des", user.description);
                    Log.d("followers ", String.valueOf(user.followersCount));
                    Log.d("createdAt", user.createdAt);
                    String id = user.idStr;
                    String name = user.name;

                    twSignInWebservice(id,name);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(TwitterException exception) {

            }
        });
    }

    public void twSignInWebservice(final String id, final String name) {

        latitude = Methodes.myLocation(TherapistSignInActivity.this).getLatitude();
        longitude = Methodes.myLocation(TherapistSignInActivity.this).getLongitude();
        final String address = getAddressFromLatLong(latitude, longitude);

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.twitterSignIn,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
//                                String email_address = jobj.getString("email_address");
                                String registerdLat = jobj.getString("latitude");
                                String registerdLong = jobj.getString("longitude");
                                SharedPreferences.Editor editor = sharedPreferences.edit();
//
                                editor.putInt("login", 2);
                                editor.putInt("user_id", user_id);
                                editor.putString("full_name", full_name);
//                                editor.putString("email_address", email_address);
                                editor.putString("registerdLat", registerdLat);
                                editor.putString("registerdLong", registerdLong);
                                editor.putString("loginWith", "TW");
                                editor.commit();
//
                                therapistDetailsWebService(user_id);
////                                Intent intent = new Intent(TherapistSignInActivity.this, TherapistTabsActivity.class);
////                                startActivity(intent);
////                                finish();
//                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();

                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(TherapistSignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(TherapistSignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("full_name", name);
//                params.put("email_address", email);
                params.put("twitter_user_id", id);
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));
                params.put("full_address", address);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }
}
