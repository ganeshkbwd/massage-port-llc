package com.hnweb.massagellc.helper;

/**
 * Created by neha on 9/22/2016.
 */
public class Urls {

    //no params
    public static String getAllTherapist = "http://designer321.com/johnilbwd/massageportllc/api/list_available_therapists.php";

    //params user_id
    public static String getAvailableDates = "http://designer321.com/johnilbwd/massageportllc/api/therapist_set_schedules.php";

    //params user_id, date
    public static String getAvailableTimings = "http://designer321.com/johnilbwd/massageportllc/api/day_available_time_schedule_of_therapist.php";

    //near therapist from user
    public static String getListofnearTherapist = "http://designer321.com/johnilbwd/massageportllc/api/list_nearby_therapists_at_customer_side.php";
    //
    public static String getReviewSubmitByCustomer = "http://designer321.com/johnilbwd/massageportllc/api/reviews_for_therapist.php";

    //customer regioster
    public static String customerRegistration = "http://designer321.com/johnilbwd/massageportllc/api/register_customer.php";

    //signin
    public static String signin = "http://designer321.com/johnilbwd/massageportllc/api/login.php";

    //forgotPassword
    public static String forgotPassword = "http://designer321.com/johnilbwd/massageportllc/api/forgotpassword.php";

    //confirm booking by customer
    public static String bookingRequestByCutomer = "http://designer321.com/johnilbwd/massageportllc/api/add_to_confirmed_bookings.php";

    //scheduledAppointments
    public static String myAppointmentsCustomer = "http://designer321.com/johnilbwd/massageportllc/api/scheduled_apts_at_customer_account.php";

    //Customer Profile
    public static String customerProfile = "http://designer321.com/johnilbwd/massageportllc/api/customer_profile.php";

    //customer profile update
    public static String updateCustomerProfile = "http://designer321.com/johnilbwd/massageportllc/api/edit_customer_profile.php";

    //getDistinctAvailableDates
    public static String getDistinctAvailableDates = "http://designer321.com/johnilbwd/massageportllc/api/get_scheduled_dates_of_therapist.php";

    //show reviews to customer
    public static String getReviewsToShowCustomer = "http://designer321.com/johnilbwd/massageportllc/api/reviews_for_therapist.php";

    //add Reviews by customer
    public static String addReviewsByCustomer = "http://designer321.com/johnilbwd/massageportllc/api/add_review_by_customer.php";

    //therapistRegistration
    public static String therapistRegistration = "http://designer321.com/johnilbwd/massageportllc/api/register_therapist.php";

    //therapist account details
    public static String therapistDetails = "http://designer321.com/johnilbwd/massageportllc/api/therapist_profile_details_in_therapist_account.php";

    //therapist appointment Details
    public static String therapistAppDetails = "http://designer321.com/johnilbwd/massageportllc/api/therapist_appointments_details.php";

    //update therapist profile
    public static String updateTherapistProfile = "http://designer321.com/johnilbwd/massageportllc/api/update_profile.php";

    //update therapist status
    public static String updateTherapistStatus = "http://designer321.com/johnilbwd/massageportllc/api/update_therapist.php";

    //Therapist My Revies
    public static String therapistMyReviews = "http://designer321.com/johnilbwd/massageportllc/api/reviews_from_customers.php";

    //Therapist Reply to review
    public static String replytocustomer = "http://designer321.com/johnilbwd/massageportllc/api/reply_to_review_by_therapist.php";

    //update therapist location
    public static String updateTherapistLocation = "http://designer321.com/johnilbwd/massageportllc/api/update_therapist_address.php";

    public static String getCurrentStatus = "http://designer321.com/johnilbwd/massageportllc/api/check_therapist_available_status.php";

    public static String getAllTherapistCustomer = "http://designer321.com/johnilbwd/massageportllc/api/list_of_all_therapists.php";

    public static String fbLoginUrl = "http://designer321.com/johnilbwd/massageportllc/api/login_with_facebook_credentials.php";

    public static String getTherapists = "http://designer321.com/johnilbwd/massageportllc/api/list_of_distinct_booked_therapists_by_customer.php";

    public static String getCustomers = "http://designer321.com/johnilbwd/massageportllc/api/list_of_distinct_booked_customers_by_therapist.php";

    public static String chatHistoryUrl = "http://designer321.com/johnilbwd/massageportllc/api/all_msgs_customer_therapist.php";

    public static String sendMsg = "http://designer321.com/johnilbwd/massageportllc/api/send_my_msg.php";

    public static String twitterSignIn = "http://designer321.com/johnilbwd/massageportllc/api/login_with_twitter_credentials.php";

    public static String getTherapistForTherapist = "http://designer321.com/johnilbwd/massageportllc/api/list_of_distinct_therapist_recipient.php";
}
