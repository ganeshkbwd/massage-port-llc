package com.hnweb.massagellc.helper;

/**
 * Created by neha on 9/2/2016.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;


public class CheckConnectivity {


    public static boolean checkInternetConnection(Context context) {

        ConnectivityManager con_manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (con_manager.getActiveNetworkInfo() != null
                && con_manager.getActiveNetworkInfo().isAvailable()
                && con_manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static void noNetMeg(Context context){
        Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
    }
}
