package com.hnweb.massagellc.helper;

import android.app.ProgressDialog;

import com.hnweb.massagellc.pojo.AvailableTimings;
import com.hnweb.massagellc.pojo.ShowTherapistData;

import java.util.ArrayList;

/**
 * Created by neha on 9/22/2016.
 */
public class ConstantVal {

    public static int MESSAGE_CODE;
    public static ProgressDialog progressDialog;
    public static String MESSAGE;
    public static String RESPONSE;
    public static int selectedPosition;
    public static String date_selected;
    public static String USER_TYPE;
    public static ArrayList<ShowTherapistData> myFileList = new ArrayList<ShowTherapistData>();
    public static String therapistPosition;
    public static String timePosition;
    public static ArrayList<AvailableTimings> myAvialtime = new ArrayList<AvailableTimings>();
    public static int BACK;
    public static double lastKnownLat;
    public static double lastKnownLongi;
    public static String reviewMsg;
    public static float myRatings;
    public static String THERAPIST_ID_REVIEW;
    public static String THERAPIST_PROFILE;
    public static ArrayList<String> avialAllreadyTimeList = new ArrayList<String>();
    public static int TOTAL_REVIEWS = 0;
    public static String AVAILABLE_STATUS;
    public static String registerdLat ;
    public static String registerdLong ;

}
