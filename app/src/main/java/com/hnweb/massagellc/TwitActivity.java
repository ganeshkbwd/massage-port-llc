package com.hnweb.massagellc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

public class TwitActivity extends AppCompatActivity {
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "bbsYIzjpNOUsfbUwOHFrhNG5V";
    private static final String TWITTER_SECRET = "f4GYWnovWkA8XtrDimTfsw1VVMy9P8oNqXSQqrPBJxkGW2gGru";

    //        private static final String TWITTER_KEY = "25IjV3s4NRPOEciZAndsKVtde";
//    private static final String TWITTER_SECRET = "966K7bD0EGYLLBdCrF2znJElmPtiFRYsRj3FQ8Cid6GBRQkF52";
    //Tags to send the username and image url to next activity using intent
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PROFILE_IMAGE_URL = "image_url";

    //Twitter Login Button
    TwitterLoginButton twitterLoginButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_twit);

        //Initializing twitter login button
        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);

        //Adding callback to the button
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                //If login succeeds passing the Calling the login method and passing Result object
                Log.e("Twit Result", result.toString());


                login(result);
            }

            @Override
            public void failure(TwitterException exception) {
                //If failure occurs while login handle it here
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });
//        initialize();
//        clickListners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Adding the login result back to the button
        Log.e("REQUST CODE", requestCode + "");
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    //The login function accepting the result object
    public void login(Result<TwitterSession> result) {

        //Creating a twitter session with result's data
        final TwitterSession session = result.data;


        //Getting the username from session
        final String username = session.getUserName();
        final TwitterAuthToken authToken = session.getAuthToken();

        String token = authToken.token;
        String secret = authToken.secret;
        Log.e("SESSION", username + "   " + authToken);

        getUserData(session, username);

    }


    public void getUserData(TwitterSession session, final String username) {
        Call<User> userResult = Twitter.getApiClient(session).getAccountService().verifyCredentials(true, false);
        userResult.enqueue(new Callback<User>() {
            @Override
            public void success(Result<User> result) {
                User user = result.data;
                String twitterImage = user.profileImageUrl;

                try {
                    System.out.println("{" + " id : " + user.idStr + " name : " + user.name + " followers : " + String.valueOf(user.followersCount) + " createdAt : " + user.createdAt + " username :" + username + "}");
                    Log.d("id", user.idStr);
//                    Log.d("email",user.idStr);
                    Log.d("name", user.name);
//                    Log.d("des", user.description);
                    Log.d("followers ", String.valueOf(user.followersCount));
                    Log.d("createdAt", user.createdAt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(TwitterException exception) {

            }
        });
    }
}

