package com.hnweb.massagellc;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Methodes;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.AvailableTimings;
import com.hnweb.massagellc.pojo.ShowTherapistData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    EditText nameET, emailET, confirm_emailET, usernameET, passET, confirm_passET, addressET;
    String name, email, confirm_email, username, pass, confirm_pass, address;
    Methodes methodes = new Methodes();
    double latitude, longitude;
    Bundle bundle;
    ArrayList<ShowTherapistData> showTherapistData = new ArrayList<ShowTherapistData>();
    String therapistPosition;
    ArrayList<AvailableTimings> availableTimingses = new ArrayList<AvailableTimings>();
    String selectedPosition;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        changeStatusBarColor();
        init();
//        getFromBundle();

    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void init() {
        nameET = (EditText) findViewById(R.id.nameET);
        emailET = (EditText) findViewById(R.id.email_idET);
        confirm_emailET = (EditText) findViewById(R.id.confirm_emailET);
        usernameET = (EditText) findViewById(R.id.userNameET);
        passET = (EditText) findViewById(R.id.passET);
        confirm_passET = (EditText) findViewById(R.id.confirmPassET);
        addressET = (EditText) findViewById(R.id.addressET);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signmeUpBTN:
                validation();
//                Toast.makeText(SignUpActivity.this, "Under development ...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.alreadyLoginBTN:
                if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Review")) {
                    Intent intent = new Intent(this, SignInActivity.class);
                    intent.putExtra("ComeFrom", "Review");
                    startActivity(intent);
                    finish();
                } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Book")) {
                    Intent intent = new Intent(this, SignInActivity.class);
                    intent.putExtra("ComeFrom", "Book");
                    startActivity(intent);
                    finish();
                } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("TP")) {
                    Intent intent = new Intent(this, SignInActivity.class);
                    intent.putExtra("ComeFrom", "TP");
                    startActivity(intent);
                    finish();
                }

                break;
        }
    }

//    public void putInBundle() {
//        b = new Bundle();
//        b.putSerializable("ShowTherapistData", showTherapistData);
//        b.putString("therapistPosition", therapistPosition);
//        b.putSerializable("AvailTimeList", availableTimingses);
//        b.putString("SelectedPosition", selectedPosition);
//    }

    public void validation() {

        name = nameET.getText().toString().trim();
        email = emailET.getText().toString().trim();
        confirm_email = confirm_emailET.getText().toString().trim();
        username = usernameET.getText().toString().trim();
        pass = passET.getText().toString().trim();
        confirm_pass = confirm_passET.getText().toString().trim();
        address = addressET.getText().toString().trim();

        if (name.length() > 0) {
            if (Patterns.EMAIL_ADDRESS.matcher(email).matches() && email.length() > 0) {
                if (Patterns.EMAIL_ADDRESS.matcher(confirm_email).matches() && email.length() > 0 && confirm_email.equals(email)) {
                    if (username.length() > 0) {
                        if (pass.length() > 0) {
                            if (confirm_pass.length() > 0 && confirm_pass.equals(pass)) {
                                if (address.length() > 0) {
                                    if (CheckConnectivity.checkInternetConnection(SignUpActivity.this)) {
                                        callSignUpWebservice(name, email, username, pass, address);
                                    } else {
                                        CheckConnectivity.noNetMeg(SignUpActivity.this);
                                    }


                                } else {
                                    addressET.setError("Please enter address.");
//                                    Toast.makeText(SignUpActivity.this, "Please enter address.", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                confirm_passET.setError("Please enter confirm password.");
//                                Toast.makeText(SignUpActivity.this, "Please enter confirm password..", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            passET.setError("Please enter password.");
//                            Toast.makeText(SignUpActivity.this, "Please enter password.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        usernameET.setError("Please enter username");
//                        Toast.makeText(SignUpActivity.this, "Please enter username.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    confirm_emailET.setError("Please enter valid confirm email id.");
//                    Toast.makeText(SignUpActivity.this, "Please enter valid confirm email id.", Toast.LENGTH_SHORT).show();
                }

            } else {
                emailET.setError("Please enter valid email id.");
//                Toast.makeText(SignUpActivity.this, "Please enter valid confirm email id.", Toast.LENGTH_SHORT).show();
            }
        } else {
            nameET.setError("Please enter name.");
//            Toast.makeText(SignUpActivity.this, "Please enter name.", Toast.LENGTH_SHORT).show();
        }


    }

    public void callSignUpWebservice(final String name, final String email, final String username, final String pass, final String address) {

        latitude = methodes.myLocation(SignUpActivity.this).getLatitude();
        longitude = methodes.myLocation(SignUpActivity.this).getLongitude();

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.customerRegistration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                Toast.makeText(SignUpActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();

                                if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Review")) {
                                    Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                                    intent.putExtra("ComeFrom", "Review");
                                    startActivity(intent);
                                    finish();
                                } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Book")) {
                                    Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                                    intent.putExtra("ComeFrom", "Book");
                                    startActivity(intent);
                                    finish();
                                } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("TP")) {
                                    Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                                    intent.putExtra("ComeFrom", "TP");
                                    startActivity(intent);
                                    finish();
                                }


                            } else {

                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(SignUpActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(SignUpActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("full_name", name);
                params.put("email_address", email);
                params.put("username", username);
                params.put("password", pass);
                params.put("latitude", String.valueOf(latitude));
                params.put("longitude", String.valueOf(longitude));
                params.put("full_address", address);
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();

    }
}
