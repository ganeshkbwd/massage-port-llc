package com.hnweb.massagellc;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hnweb.massagellc.helper.CheckConnectivity;
import com.hnweb.massagellc.helper.ConstantVal;
import com.hnweb.massagellc.helper.Urls;
import com.hnweb.massagellc.pojo.AvailableTimings;
import com.hnweb.massagellc.pojo.ShowTherapistData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {
    EditText email_idET, passET;
    String email, pass;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MassagePortLLCAppPrefs";
    Bundle bundle;
    ArrayList<ShowTherapistData> showTherapistData = new ArrayList<ShowTherapistData>();
    String therapistPosition;
    ArrayList<AvailableTimings> availableTimingses = new ArrayList<AvailableTimings>();
    String selectedPosition;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        changeStatusBarColor();
        init();


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.black));
        }
    }

    public void init() {
        email_idET = (EditText) findViewById(R.id.userNameET);
        passET = (EditText) findViewById(R.id.passET);
//        getFromBundle();

    }

//    public void getFromBundle() {
//        bundle = getIntent().getExtras();
//        showTherapistData = (ArrayList<ShowTherapistData>) bundle.getSerializable("ShowTherapistData");
//        therapistPosition = bundle.getString("Position");
//        availableTimingses = (ArrayList<AvailableTimings>) bundle.getSerializable("AvailTimeList");
//        selectedPosition = bundle.getString("SelectedPosition");
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgot_passBTN:
//                Toast.makeText(this, "Under Development....", Toast.LENGTH_SHORT).show();
                forgotPasswordDialog();
                break;
            case R.id.signupBTN:
                if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Review")) {
                    intent = new Intent(this, SignUpActivity.class);
                    intent.putExtra("ComeFrom", "Review");
                    startActivity(intent);
                    finish();
                } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Book")) {

                    intent = new Intent(this, SignUpActivity.class);
                    intent.putExtra("ComeFrom", "Book");
                    startActivity(intent);
                    finish();
                }else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("TP")) {
                    intent = new Intent(this, SignUpActivity.class);
                    intent.putExtra("ComeFrom", "TP");
                    startActivity(intent);
                    finish();
                }
                break;
            case R.id.loginBTN:
                email = email_idET.getText().toString().trim();
                pass = passET.getText().toString().trim();


                if (email.length() > 0) {
                    if (pass.length() > 0) {
                        if (CheckConnectivity.checkInternetConnection(SignInActivity.this)) {
                            callSignInWebservice(email, pass, availableTimingses, selectedPosition, showTherapistData, therapistPosition);
                        } else {
                            CheckConnectivity.noNetMeg(SignInActivity.this);
                        }

                    } else {
                        passET.setError("Please enter password.");
                    }
                } else {
                    email_idET.setError("Please enter username.");
                }

                break;
        }
    }

    public void forgotPasswordDialog() {
        final Dialog settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(this.getLayoutInflater().inflate(R.layout.forgot_password
                , null));
        settingsDialog.setCancelable(true);
        final EditText fppassTV = (EditText) settingsDialog.findViewById(R.id.fp_email_idET);
        Button submitBTN = (Button) settingsDialog.findViewById(R.id.fpsubmitBTN);
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fppassTV.getText().toString().trim().length() > 0 && Patterns.EMAIL_ADDRESS.matcher(fppassTV.getText().toString().trim()).matches()) {
                    String email = fppassTV.getText().toString().trim();
                    if (CheckConnectivity.checkInternetConnection(SignInActivity.this)) {
                        callForgotPasswordWebService(email, settingsDialog);
                    } else {
                        CheckConnectivity.noNetMeg(SignInActivity.this);
                    }


//                    Toast.makeText(SignInActivity.this, "Under Development ....", Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(SignInActivity.this, "Please enter your correct email id", Toast.LENGTH_SHORT).show();
                }
            }
        });

        settingsDialog.show();
    }


    public void callSignInWebservice(final String username, final String pass, final ArrayList<AvailableTimings> availableTimingses, final String selectedPosition, final ArrayList<ShowTherapistData> showTherapistData, final String therapistPosition) {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.signin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            ConstantVal.MESSAGE = jobj.getString("message");


                            if (ConstantVal.MESSAGE_CODE == 1) {
                                int user_id = jobj.getInt("user_id");
                                String full_name = jobj.getString("full_name");
                                String email_address = jobj.getString("email_address");
                                SharedPreferences.Editor editor = sharedPreferences.edit();

                                editor.putInt("login", 1);
                                editor.putInt("user_id", user_id);
                                editor.putString("full_name", full_name);
                                editor.putString("email_address", email_address);
                                editor.putString("loginWith", "No");
                                editor.commit();

                                Toast.makeText(SignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();

                                if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("Review")) {

                                    Bundle bundle = new Bundle();
                                    bundle.putString("ComeFrom", "SignIn");
                                    intent = new Intent(SignInActivity.this, ReviewCustomerSideActivity.class);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } else if (getIntent().getExtras().getString("ComeFrom").equalsIgnoreCase("TP")) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("ComeFrom", "TP");
                                    intent = new Intent(SignInActivity.this, TherapistProfileActivity.class);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } else {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("ComeFrom", "SignIn");
                                    intent = new Intent(SignInActivity.this, TherapistProfileActivity.class);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }


                                ConstantVal.progressDialog.dismiss();


                            } else {

                                ConstantVal.progressDialog.dismiss();//
                                Toast.makeText(SignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(SignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("user_type", "Customer");
                params.put("username", username);
                params.put("password", pass);
                Log.e("PARAMS", params.toString());
                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }


    public void callForgotPasswordWebService(final String email, final Dialog settingsDialog) {

        ConstantVal.progressDialog = new ProgressDialog(this);
        ConstantVal.progressDialog.setMessage("Please wait ....");
        ConstantVal.progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.forgotPassword,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            ConstantVal.MESSAGE_CODE = jobj.getInt("message_code");
                            if (ConstantVal.MESSAGE_CODE == 1) {
                                ConstantVal.MESSAGE = jobj.getString("message");
                                Toast.makeText(SignInActivity.this, ConstantVal.MESSAGE, Toast.LENGTH_SHORT).show();
                                ConstantVal.progressDialog.dismiss();
                                settingsDialog.dismiss();

                            } else {

                                ConstantVal.progressDialog.dismiss();
                                Toast.makeText(SignInActivity.this, "Your entered email id not registered.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ConstantVal.progressDialog.dismiss();
                Toast.makeText(SignInActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                Map<String, String> params = new Hashtable<String, String>();
                params.put("email_address", email);

                //returning parameters
                return params;
            }
        };

        queue.add(stringRequest);


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        finish();
    }
}
